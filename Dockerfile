FROM openjdk:8-alpine
ADD target/msproducts-1.0.jar /usr/share/msproducts-1.0.jar
EXPOSE  8087
ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/msproducts-1.0.jar"]