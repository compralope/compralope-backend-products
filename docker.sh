#!/bin/bash

git pull
git checkout .
git checkout $1
git pull
mvn clean package
docker rm -f ms_products
docker rmi ms_products
docker build -t ms_products .
docker run -p 8087:8087 --name ms_products --link RBC:database -d ms_products
docker ps -a

exit 0