package pe.compralo.products.cross.variables;

public class Parameters {
	public static final int Currency_Soles_Id = 1;
	public static final String Currency_Soles_Title = "S/.";
	
	
	public static final int State_Product_Active = 1;
	public static final int State_Product_Inactive = 2;
	
	
	public static final String Path_Order_Product = "order/product";
	public static final String Separator_Order_Product = "|";
	
	public static final int Product_Inbox_Search_Type_Out_Of_Stock = 1;
	public static final int Product_Inbox_Search_Type_Enabled = 2;
	public static final int Product_Inbox_Search_Type_Disabled = 3;
	public static final int Product_Inbox_Search_Type_Sorting_Title_Asc = 4;
	public static final int Product_Inbox_Search_Type_Sorting_Title_Desc = 5;
	public static final int Product_Inbox_Search_Type_Sorting_Price_Asc = 6;
	public static final int Product_Inbox_Search_Type_Sorting_Price_Desc = 7;
}
