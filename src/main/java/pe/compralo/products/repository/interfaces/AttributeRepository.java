package pe.compralo.products.repository.interfaces;

import java.util.List;
import java.util.Map;

import pe.compralo.products.application.domain.Attribute;
import pe.compralo.products.application.domain.Pagination;

public interface AttributeRepository {

	Attribute getSearch(Map<String,Object> parametersJson);

	List<Attribute> getSearch(Map<String,Object> parametersJson,Pagination pagination);

	List<Attribute> getFindAll(Map<String,Object> parametersJson,Pagination pagination);

	void setInsertUpdate(Attribute o);
	
	int setInsertUpdateMassive(List<Attribute> list);

}