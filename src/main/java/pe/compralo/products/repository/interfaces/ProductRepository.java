package pe.compralo.products.repository.interfaces;

import java.util.List;
import java.util.Map;

import pe.compralo.products.application.domain.Product;
import pe.compralo.products.application.domain.Pagination;

public interface ProductRepository {

	Product getSearch(Map<String,Object> parametersJson);

	List<Product> getSearch(Map<String,Object> parametersJson,Pagination pagination);

	List<Product> getFindAll(Map<String,Object> parametersJson,Pagination pagination);

	void setInsertUpdate(Product o);

}