package pe.compralo.products.repository.interfaces;

import java.util.List;
import java.util.Map;

import pe.compralo.products.application.domain.Category;
import pe.compralo.products.application.domain.Pagination;

public interface CategoryRepository {

	Category getSearch(Map<String,Object> parametersJson);

	List<Category> getSearch(Map<String,Object> parametersJson,Pagination pagination);

	List<Category> getFindAll(Map<String,Object> parametersJson,Pagination pagination);

	void setInsertUpdate(Category o);

}