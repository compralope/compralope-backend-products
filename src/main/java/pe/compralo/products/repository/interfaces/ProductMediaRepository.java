package pe.compralo.products.repository.interfaces;

import java.util.List;
import java.util.Map;

import pe.compralo.products.application.domain.ProductMedia;
import pe.compralo.products.application.domain.Pagination;

public interface ProductMediaRepository {

	ProductMedia getSearch(Map<String,Object> parametersJson);

	List<ProductMedia> getSearch(Map<String,Object> parametersJson,Pagination pagination);

	List<ProductMedia> getFindAll(Map<String,Object> parametersJson,Pagination pagination);

	void setInsertUpdate(ProductMedia o);

	int setInsertUpdateMassive(List<ProductMedia> list);	
}