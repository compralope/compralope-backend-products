package pe.compralo.products.repository.interfaces;

import java.util.List;
import java.util.Map;

import pe.compralo.products.application.domain.VariantDetail;
import pe.compralo.products.application.domain.Pagination;

public interface VariantDetailRepository {

	VariantDetail getSearch(Map<String,Object> parametersJson);

	List<VariantDetail> getSearch(Map<String,Object> parametersJson,Pagination pagination);

	List<VariantDetail> getFindAll(Map<String,Object> parametersJson,Pagination pagination);

	void setInsertUpdate(VariantDetail o);
	
	int setInsertUpdateMassive(List<VariantDetail> list);

}