package pe.compralo.products.repository.interfaces;

import java.util.List;
import java.util.Map;

import pe.compralo.products.application.domain.AttributeItems;
import pe.compralo.products.application.domain.Pagination;

public interface AttributeItemsRepository {

	AttributeItems getSearch(Map<String,Object> parametersJson);

	List<AttributeItems> getSearch(Map<String,Object> parametersJson,Pagination pagination);

	List<AttributeItems> getFindAll(Map<String,Object> parametersJson,Pagination pagination);

	void setInsertUpdate(AttributeItems o);

	int setInsertUpdateMassive(List<AttributeItems> list);
}