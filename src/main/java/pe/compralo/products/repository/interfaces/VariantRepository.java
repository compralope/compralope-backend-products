package pe.compralo.products.repository.interfaces;

import java.util.List;
import java.util.Map;

import pe.compralo.products.application.domain.Variant;
import pe.compralo.products.application.domain.Pagination;

public interface VariantRepository {

	Variant getSearch(Map<String,Object> parametersJson);

	List<Variant> getSearch(Map<String,Object> parametersJson,Pagination pagination);

	List<Variant> getFindAll(Map<String,Object> parametersJson,Pagination pagination);

	void setInsertUpdate(Variant o);

	int setInsertUpdateMassive(List<Variant> list);
}