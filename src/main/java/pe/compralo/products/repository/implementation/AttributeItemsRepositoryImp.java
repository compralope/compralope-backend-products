package pe.compralo.products.repository.implementation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.repository.interfaces.SQLHelper;
import pe.compralo.products.application.domain.AttributeItems;
import pe.compralo.products.repository.interfaces.AttributeItemsRepository;

@Repository
public class AttributeItemsRepositoryImp implements AttributeItemsRepository {

	@Autowired
	private SQLHelper sqlHelper;

	@Override
	public AttributeItems getSearch(Map<String,Object> parametersJson) {
		AttributeItems o = sqlHelper.getSearch("dbo.attribute_items_search", parametersJson, new AttributeItemsSearchRowMapper());

		return o;
	}

	@Override
	public List<AttributeItems> getSearch(Map<String,Object> parametersJson, Pagination pagination) {
		List<AttributeItems> l = sqlHelper.getSearch("dbo.attribute_items_search", parametersJson, pagination, new AttributeItemsSearchRowMapper());

		return l;
	}

	static class AttributeItemsSearchRowMapper implements RowMapper<AttributeItems> {
		@Override
		public AttributeItems mapRow(ResultSet r, int row) throws SQLException {
			AttributeItems o = new AttributeItems();

			o.setId(r.getInt("id"));
			o.setAttributeId(r.getInt("attribute_id"));
			o.setTitle(r.getString("title"));
			o.setActive(r.getBoolean("active"));

			return o;
		}
	}

	@Override
	public List<AttributeItems> getFindAll(Map<String,Object> parametersJson, Pagination pagination) {
		List<AttributeItems> l = sqlHelper.getFindAll("dbo.attribute_items_find_all", parametersJson, pagination, new AttributeItemsFindAllRowMapper());

		return l;
	}

	static class AttributeItemsFindAllRowMapper implements RowMapper<AttributeItems> {
		@Override
		public AttributeItems mapRow(ResultSet r, int row) throws SQLException {
			AttributeItems o = new AttributeItems();

			o.setId(r.getInt("id"));			
			o.setAttributeId(r.getInt("attribute_id"));
			o.setTitle(r.getString("title"));
			o.setActive(r.getBoolean("active"));

			return o;
		}
	}

	@Override
	public void setInsertUpdate(AttributeItems o) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("id", o.getId());
		parameters.put("attribute_id", o.getAttributeId());
		parameters.put("title", o.getTitle());
		parameters.put("active", o.getActive());

		int id = sqlHelper.setInsertUpdate("dbo.attribute_items_insert_update", parameters);

		o.setId(id);
	}

	
	@Override
	public int setInsertUpdateMassive(List<AttributeItems> list) {
		return sqlHelper.setInsertUpdateMassive("dbo.attribute_items_insert_update_massive", list);
	}		
}