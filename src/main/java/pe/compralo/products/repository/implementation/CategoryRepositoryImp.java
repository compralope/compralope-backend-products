package pe.compralo.products.repository.implementation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.cross.utils.ConvertFormat;
import pe.compralo.products.repository.interfaces.SQLHelper;
import pe.compralo.products.application.domain.Category;
import pe.compralo.products.repository.interfaces.CategoryRepository;

@Repository
public class CategoryRepositoryImp implements CategoryRepository {

	@Autowired
	private SQLHelper sqlHelper;

	@Override
	public Category getSearch(Map<String,Object> parametersJson) {
		Category o = sqlHelper.getSearch("dbo.category_search", parametersJson, new CategorySearchRowMapper());

		return o;
	}

	@Override
	public List<Category> getSearch(Map<String,Object> parametersJson, Pagination pagination) {
		List<Category> l = sqlHelper.getSearch("dbo.category_search", parametersJson, pagination, new CategorySearchRowMapper());

		return l;
	}

	static class CategorySearchRowMapper implements RowMapper<Category> {
		@Override
		public Category mapRow(ResultSet r, int row) throws SQLException {
			Category o = new Category();

			o.setId(r.getInt("id"));
			o.setTenantId(r.getInt("tenant_id"));
			o.setCategoryParentId(r.getInt("category_parent_id"));
			o.setTitle(r.getString("title"));
			o.setActive(r.getBoolean("active"));

			return o;
		}
	}

	@Override
	public List<Category> getFindAll(Map<String,Object> parametersJson, Pagination pagination) {
		List<Category> l = sqlHelper.getFindAll("dbo.category_find_all", parametersJson, pagination, new CategoryFindAllRowMapper());

		return l;
	}

	static class CategoryFindAllRowMapper implements RowMapper<Category> {
		@Override
		public Category mapRow(ResultSet r, int row) throws SQLException {
			Category o = new Category();

			o.setId(r.getInt("id"));
			o.setTenantId(r.getInt("tenant_id"));
			o.setCategoryParentId(r.getInt("category_parent_id"));
			o.setTitle(r.getString("title"));
			o.setActive(r.getBoolean("active"));

			return o;
		}
	}

	@Override
	public void setInsertUpdate(Category o) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("id", o.getId());
		parameters.put("tenant_id", o.getTenantId());
		parameters.put("category_parent_id", ConvertFormat.IntegerToNull(o.getCategoryParentId()));
		parameters.put("title", o.getTitle());
		parameters.put("active", o.getActive());

		int id = sqlHelper.setInsertUpdate("dbo.category_insert_update", parameters);

		o.setId(id);
	}

}