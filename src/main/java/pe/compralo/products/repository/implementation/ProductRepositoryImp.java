package pe.compralo.products.repository.implementation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.repository.interfaces.SQLHelper;
import pe.compralo.products.application.domain.Product;
import pe.compralo.products.cross.variables.Parameters;
import pe.compralo.products.repository.interfaces.ProductRepository;

@Repository
public class ProductRepositoryImp implements ProductRepository {

	@Autowired
	private SQLHelper sqlHelper;

	@Override
	public Product getSearch(Map<String,Object> parametersJson) {
		Product o = sqlHelper.getSearch("dbo.product_search", parametersJson, new ProductSearchRowMapper());

		return o;
	}

	@Override
	public List<Product> getSearch(Map<String,Object> parametersJson, Pagination pagination) {
		List<Product> l = sqlHelper.getSearch("dbo.product_search", parametersJson, pagination, new ProductSearchRowMapper());

		return l;
	}

	static class ProductSearchRowMapper implements RowMapper<Product> {
		@Override
		public Product mapRow(ResultSet r, int row) throws SQLException {
			Product o = new Product();

			o.setId(r.getInt("id"));
			o.setTenantId(r.getInt("tenant_id"));
			o.setStateId(r.getInt("state_id"));
			o.setStateTitle(r.getString("state_title"));
			o.setCategoryId(r.getInt("category_id"));
			o.setCategoryTitle(r.getString("category_title"));
			o.setCurrencyId(r.getInt("currency_id"));
			o.setCurrencyTitle(Parameters.Currency_Soles_Title);
			o.setCode(r.getString("code"));
			o.setTitle(r.getString("title"));
			o.setDetails(r.getString("details"));
			o.setUrl(r.getString("url"));
			o.setEnableIgv(r.getBoolean("enable_igv"));
			o.setEnableVariant(r.getBoolean("enable_variant"));
			o.setActive(r.getBoolean("active"));
			o.setRegisterDate(r.getTimestamp("register_date"));
			o.setUpdateDate(r.getTimestamp("update_date"));

			return o;
		}
	}

	@Override
	public List<Product> getFindAll(Map<String,Object> parametersJson, Pagination pagination) {
		List<Product> l = sqlHelper.getFindAll("dbo.product_find_all", parametersJson, pagination, new ProductFindAllRowMapper());

		return l;
	}

	static class ProductFindAllRowMapper implements RowMapper<Product> {
		@Override
		public Product mapRow(ResultSet r, int row) throws SQLException {
			Product o = new Product();

			o.setId(r.getInt("id"));
			o.setTenantId(r.getInt("tenant_id"));
			o.setStateId(r.getInt("state_id"));
			o.setStateTitle(r.getString("state_title"));
			o.setCategoryId(r.getInt("category_id"));
			o.setCategoryTitle(r.getString("category_title"));
			o.setCurrencyId(r.getInt("currency_id"));
			o.setCurrencyTitle(Parameters.Currency_Soles_Title);
			o.setStock(r.getDouble("stock"));
			o.setSellingPriceMax(r.getDouble("selling_price_max"));
			o.setSellingPriceMin(r.getDouble("selling_price_min"));
			o.setCode(r.getString("code"));
			o.setTitle(r.getString("title"));
			o.setUrl(r.getString("url"));
			o.setUrlImage(r.getString("url_image"));
			o.setUrlThumbnail(r.getString("url_thumbnail"));			
			o.setEnableIgv(r.getBoolean("enable_igv"));
			o.setEnableVariant(r.getBoolean("enable_variant"));
			o.setActive(r.getBoolean("active"));
			return o;
		}
	}

	@Override
	public void setInsertUpdate(Product o) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("id", o.getId());
		parameters.put("tenant_id", o.getTenantId());
		parameters.put("state_id", o.getStateId());
		parameters.put("category_id", o.getCategoryId());
		parameters.put("currency_id", o.getCurrencyId());
		parameters.put("code", o.getCode());
		parameters.put("title", o.getTitle());
		parameters.put("details", o.getDetails());
		parameters.put("url", o.getUrl());
		parameters.put("enable_igv", o.getEnableIgv());
		parameters.put("enable_variant", o.getEnableVariant());
		parameters.put("active", o.getActive());
		parameters.put("register_date", o.getRegisterDate());
		parameters.put("update_date", o.getUpdateDate());

		int id = sqlHelper.setInsertUpdate("dbo.product_insert_update", parameters);

		o.setId(id);
	}

}