package pe.compralo.products.repository.implementation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.repository.interfaces.SQLHelper;
import pe.compralo.products.application.domain.VariantDetail;
import pe.compralo.products.repository.interfaces.VariantDetailRepository;

@Repository
public class VariantDetailRepositoryImp implements VariantDetailRepository {

	@Autowired
	private SQLHelper sqlHelper;

	@Override
	public VariantDetail getSearch(Map<String,Object> parametersJson) {
		VariantDetail o = sqlHelper.getSearch("dbo.variant_detail_search", parametersJson, new VariantDetailSearchRowMapper());

		return o;
	}

	@Override
	public List<VariantDetail> getSearch(Map<String,Object> parametersJson, Pagination pagination) {
		List<VariantDetail> l = sqlHelper.getSearch("dbo.variant_detail_search", parametersJson, pagination, new VariantDetailSearchRowMapper());

		return l;
	}

	static class VariantDetailSearchRowMapper implements RowMapper<VariantDetail> {
		@Override
		public VariantDetail mapRow(ResultSet r, int row) throws SQLException {
			VariantDetail o = new VariantDetail();

			o.setId(r.getInt("id"));
			o.setVariantId(r.getInt("variant_id"));
			o.setAttributeId(r.getInt("atrribute_id"));
			o.setAttributeTitle(r.getString("atrribute_title"));
			o.setAttributeItemsId(r.getInt("attribute_items_id"));
			o.setAttributeItemsTitle(r.getString("atrribute_items_title"));
			o.setActive(r.getBoolean("active"));
			
			return o;
		}
	}

	@Override
	public List<VariantDetail> getFindAll(Map<String,Object> parametersJson, Pagination pagination) {
		List<VariantDetail> l = sqlHelper.getFindAll("dbo.variant_detail_find_all", parametersJson, pagination, new VariantDetailFindAllRowMapper());

		return l;
	}

	static class VariantDetailFindAllRowMapper implements RowMapper<VariantDetail> {
		@Override
		public VariantDetail mapRow(ResultSet r, int row) throws SQLException {
			VariantDetail o = new VariantDetail();

			o.setId(r.getInt("id"));
			o.setVariantId(r.getInt("variant_id"));
			o.setAttributeItemsId(r.getInt("attribute_items_id"));

			return o;
		}
	}

	@Override
	public void setInsertUpdate(VariantDetail o) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("id", o.getId());
		parameters.put("variant_id", o.getVariantId());
		parameters.put("attribute_items_id", o.getAttributeItemsId());

		int id = sqlHelper.setInsertUpdate("dbo.variant_detail_insert_update", parameters);

		o.setId(id);
	}
	
	@Override
	public int setInsertUpdateMassive(List<VariantDetail> list) {
		return sqlHelper.setInsertUpdateMassive("dbo.variant_detail_insert_update_massive", list);
	}	

}