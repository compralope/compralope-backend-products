package pe.compralo.products.repository.implementation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.repository.interfaces.SQLHelper;
import pe.compralo.products.application.domain.Variant;
import pe.compralo.products.repository.interfaces.VariantRepository;

@Repository
public class VariantRepositoryImp implements VariantRepository {

	@Autowired
	private SQLHelper sqlHelper;

	@Override
	public Variant getSearch(Map<String,Object> parametersJson) {
		Variant o = sqlHelper.getSearch("dbo.variant_search", parametersJson, new VariantSearchRowMapper());

		return o;
	}

	@Override
	public List<Variant> getSearch(Map<String,Object> parametersJson, Pagination pagination) {
		List<Variant> l = sqlHelper.getSearch("dbo.variant_search", parametersJson, pagination, new VariantSearchRowMapper());

		return l;
	}

	static class VariantSearchRowMapper implements RowMapper<Variant> {
		@Override
		public Variant mapRow(ResultSet r, int row) throws SQLException {
			Variant o = new Variant();

			o.setId(r.getInt("id"));
			o.setProductId(r.getInt("product_id"));
			o.setStateId(r.getInt("state_id"));
			o.setCode(r.getString("code"));
			o.setTitle(r.getString("title"));
			o.setSellingPrice(r.getDouble("selling_price"));
			o.setCostPrice(r.getDouble("cost_price"));
			o.setCostAverage(r.getDouble("cost_average"));
			o.setStock(r.getDouble("stock"));
			o.setActive(r.getBoolean("active"));

			return o;
		}
	}

	@Override
	public List<Variant> getFindAll(Map<String,Object> parametersJson, Pagination pagination) {
		List<Variant> l = sqlHelper.getFindAll("dbo.variant_find_all", parametersJson, pagination, new VariantFindAllRowMapper());

		return l;
	}

	static class VariantFindAllRowMapper implements RowMapper<Variant> {
		@Override
		public Variant mapRow(ResultSet r, int row) throws SQLException {
			Variant o = new Variant();

			o.setId(r.getInt("id"));
			o.setProductId(r.getInt("product_id"));
			o.setStateId(r.getInt("state_id"));
			o.setTitle(r.getString("title"));
			o.setSellingPrice(r.getDouble("selling_price"));
			o.setCostPrice(r.getDouble("cost_price"));
			o.setCostAverage(r.getDouble("cost_average"));
			o.setStock(r.getDouble("stock"));
			o.setActive(r.getBoolean("active"));

			return o;
		}
	}

	@Override
	public void setInsertUpdate(Variant o) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("id", o.getId());
		parameters.put("product_id", o.getProductId());
		parameters.put("state_id", o.getStateId());
		parameters.put("title", o.getTitle());
		parameters.put("selling_price", o.getSellingPrice());
		parameters.put("cost_price", o.getCostPrice());
		parameters.put("cost_average", o.getCostAverage());
		parameters.put("stock", o.getStock());
		parameters.put("active", o.getActive());

		int id = sqlHelper.setInsertUpdate("dbo.variant_insert_update", parameters);

		o.setId(id);
	}

	@Override
	public int setInsertUpdateMassive(List<Variant> list) {
		return sqlHelper.setInsertUpdateMassive("dbo.variant_insert_update_massive", list);
	}

}