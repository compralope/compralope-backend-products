package pe.compralo.products.repository.implementation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.repository.interfaces.SQLHelper;
import pe.compralo.products.application.domain.Attribute;
import pe.compralo.products.repository.interfaces.AttributeRepository;

@Repository
public class AttributeRepositoryImp implements AttributeRepository {

	@Autowired
	private SQLHelper sqlHelper;

	@Override
	public Attribute getSearch(Map<String,Object> parametersJson) {
		Attribute o = sqlHelper.getSearch("dbo.attribute_search", parametersJson, new AttributeSearchRowMapper());

		return o;
	}

	@Override
	public List<Attribute> getSearch(Map<String,Object> parametersJson, Pagination pagination) {
		List<Attribute> l = sqlHelper.getSearch("dbo.attribute_search", parametersJson, pagination, new AttributeSearchRowMapper());

		return l;
	}

	static class AttributeSearchRowMapper implements RowMapper<Attribute> {
		@Override
		public Attribute mapRow(ResultSet r, int row) throws SQLException {
			Attribute o = new Attribute();

			o.setId(r.getInt("id"));
			o.setTitle(r.getString("title"));
			o.setActive(r.getBoolean("active"));

			return o;
		}
	}

	@Override
	public List<Attribute> getFindAll(Map<String,Object> parametersJson, Pagination pagination) {
		List<Attribute> l = sqlHelper.getFindAll("dbo.attribute_find_all", parametersJson, pagination, new AttributeFindAllRowMapper());

		return l;
	}

	static class AttributeFindAllRowMapper implements RowMapper<Attribute> {
		@Override
		public Attribute mapRow(ResultSet r, int row) throws SQLException {
			Attribute o = new Attribute();

			o.setId(r.getInt("id"));
			o.setTitle(r.getString("title"));
			o.setActive(r.getBoolean("active"));

			return o;
		}
	}

	@Override
	public void setInsertUpdate(Attribute o) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("id", o.getId());
		parameters.put("title", o.getTitle());
		parameters.put("active", o.getActive());

		int id = sqlHelper.setInsertUpdate("dbo.attribute_insert_update", parameters);

		o.setId(id);
	}
	
	@Override
	public int setInsertUpdateMassive(List<Attribute> list) {
		return sqlHelper.setInsertUpdateMassive("dbo.attribute_insert_update_massive", list);
	}	

}