package pe.compralo.products.repository.implementation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.repository.interfaces.SQLHelper;
import pe.compralo.products.application.domain.ProductMedia;
import pe.compralo.products.application.domain.VariantDetail;
import pe.compralo.products.repository.interfaces.ProductMediaRepository;

@Repository
public class ProductMediaRepositoryImp implements ProductMediaRepository {

	@Autowired
	private SQLHelper sqlHelper;

	@Override
	public ProductMedia getSearch(Map<String,Object> parametersJson) {
		ProductMedia o = sqlHelper.getSearch("dbo.product_media_search", parametersJson, new ProductMediaSearchRowMapper());

		return o;
	}

	@Override
	public List<ProductMedia> getSearch(Map<String,Object> parametersJson, Pagination pagination) {
		List<ProductMedia> l = sqlHelper.getSearch("dbo.product_media_search", parametersJson, pagination, new ProductMediaSearchRowMapper());

		return l;
	}

	static class ProductMediaSearchRowMapper implements RowMapper<ProductMedia> {
		@Override
		public ProductMedia mapRow(ResultSet r, int row) throws SQLException {
			ProductMedia o = new ProductMedia();

			o.setId(r.getInt("id"));
			o.setProductId(r.getInt("product_id"));
			o.setFileStorageId(r.getInt("file_storage_id"));
			o.setImageType(r.getInt("image_type"));
			o.setUrl(r.getString("url"));
			o.setOrderIndex(r.getInt("order_index"));
			o.setPrincipal(r.getBoolean("principal"));
			o.setEnable(r.getBoolean("enable"));

			return o;
		}
	}

	@Override
	public List<ProductMedia> getFindAll(Map<String,Object> parametersJson, Pagination pagination) {
		List<ProductMedia> l = sqlHelper.getFindAll("dbo.product_media_find_all", parametersJson, pagination, new ProductMediaFindAllRowMapper());

		return l;
	}

	static class ProductMediaFindAllRowMapper implements RowMapper<ProductMedia> {
		@Override
		public ProductMedia mapRow(ResultSet r, int row) throws SQLException {
			ProductMedia o = new ProductMedia();

			o.setId(r.getInt("id"));
			o.setProductId(r.getInt("product_id"));
			o.setFileStorageId(r.getInt("file_storage_id"));
			o.setImageType(r.getInt("image_type"));
			o.setUrl(r.getString("url"));
			o.setOrderIndex(r.getInt("order_index"));
			o.setPrincipal(r.getBoolean("principal"));
			o.setEnable(r.getBoolean("enable"));

			return o;
		}
	}

	@Override
	public void setInsertUpdate(ProductMedia o) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("id", o.getId());
		parameters.put("product_id", o.getProductId());
		parameters.put("media_id", o.getFileStorageId());
		parameters.put("order_index", o.getOrderIndex());
		parameters.put("enable", o.getEnable());

		int id = sqlHelper.setInsertUpdate("dbo.product_media_insert_update", parameters);

		o.setId(id);
	}

	@Override
	public int setInsertUpdateMassive(List<ProductMedia> list) {
		return sqlHelper.setInsertUpdateMassive("dbo.product_media_insert_update_massive", list);
	}		
	
}