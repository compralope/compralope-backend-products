package pe.compralo.products.api.query;

public class VariantDetailQuery extends PaginationQuery
{

	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	private int variantId;
	public int getVariantId() {
		return variantId;
	}
	public void setVariantId(int variantId) {
		this.variantId = variantId;
	}

	private int attributeItemsId;
	public int getAttributeItemsId() {
		return attributeItemsId;
	}
	public void setAttributeItemsId(int atrributeItemsId) {
		this.attributeItemsId = atrributeItemsId;
	}

}