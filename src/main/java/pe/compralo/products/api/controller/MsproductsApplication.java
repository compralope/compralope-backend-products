package pe.compralo.products.api.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "pe.compralo.products")
public class MsproductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsproductsApplication.class, args);
	}

}
