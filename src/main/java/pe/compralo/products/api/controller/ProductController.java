package pe.compralo.products.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.compralo.products.api.response.PaginationResponse;
import pe.compralo.products.api.response.ProductInboxResponse;
import pe.compralo.products.api.query.ProductQuery;
import pe.compralo.products.api.request.ProductRequest;
import pe.compralo.products.api.response.ProductResponse;
import pe.compralo.products.application.domain.Messenger;
import pe.compralo.products.application.domain.Product;
import pe.compralo.products.application.mapper.MessengerMapper;
import pe.compralo.products.application.mapper.ProductMapper;
import pe.compralo.products.service.interfaces.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private ProductService productService;

	@GetMapping("/{id}")
	public ResponseEntity<ProductResponse> getSelect(@PathVariable int id) {
		Product o = productService.getSelectView(id);

		if(o==null) return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);

		return new ResponseEntity<ProductResponse>(ProductMapper.ToSelectProductResponse(o),HttpStatus.OK);
	}

	@GetMapping("/search")
	public ResponseEntity<List<ProductResponse>> getSelect(ProductQuery q) {
		List<Product> l = productService.getSelect(q.getId(),q.getPagination());

		return new ResponseEntity<List<ProductResponse>>(ProductMapper.ToArraySelectProductResponse(l),HttpStatus.OK);
	}

	@GetMapping("/search/exists")
	public ResponseEntity<ProductResponse> getSelectExists(ProductQuery q) {
		Product exists = productService.getSelectByCode(q.getTenantId(), q.getCode());
		
		if(exists==null) return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);

		return new ResponseEntity<ProductResponse>(ProductMapper.ToSelectProductResponse(exists),HttpStatus.OK);
	}	

	@GetMapping("/find-all")
	public ResponseEntity<PaginationResponse<ProductResponse>> getList(ProductQuery q) {
		List<Product> l = productService.getList(q.getId(),q.getPagination());

		return new ResponseEntity<PaginationResponse<ProductResponse>>(new PaginationResponse<ProductResponse>(q.getPagination(),ProductMapper.ToArrayListProductResponse(l)),HttpStatus.OK);
	}
	
	@GetMapping("/find-all/inbox")
	public ResponseEntity<PaginationResponse<ProductInboxResponse>> getListByInbox(ProductQuery q) {
		List<Product> l = productService.getListByInbox(q.getTenantId(),q.getTitle(),q.getListCategoryId(),q.getSearchType(),q.getPagination());

		return new ResponseEntity<PaginationResponse<ProductInboxResponse>>(new PaginationResponse<ProductInboxResponse>(q.getPagination(),ProductMapper.ToArrayListByInboxProductResponse(l)),HttpStatus.OK);
	}	
	
	@GetMapping("/find-all/autocomplete")
	public ResponseEntity<List<ProductInboxResponse>> getListAutocompleteByTenant(ProductQuery q) {
		List<Product> l = productService.getListAutocompleteByTenant(q.getTenantId(),q.getPagination());

		return new ResponseEntity<List<ProductInboxResponse>>(ProductMapper.ToArrayListByInboxProductResponse(l),HttpStatus.OK);
	}		

	@PostMapping()
	public ResponseEntity<Object> setRegister(@RequestBody ProductRequest r) {		
		Product o = ProductMapper.FromRegisterProductRequest(r);

		Messenger msg = productService.setRegister(o);
		
		if(msg!=null) return new ResponseEntity<>(MessengerMapper.ToSelectMessageResponse(msg),HttpStatus.BAD_REQUEST);
		
		return new ResponseEntity<>(ProductMapper.ToRegisterProductResponse(o),HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<ProductResponse> setUpdate(@PathVariable int id,@RequestBody ProductRequest r) {
		Product o = ProductMapper.FromRegisterProductRequest(r);

		productService.setUpdate(id,o);

		return new ResponseEntity<ProductResponse>(ProductMapper.ToRegisterProductResponse(o),HttpStatus.OK);
	}
	
	@PatchMapping("/state/{id}")
	public ResponseEntity<ProductResponse> setUpdateState(@PathVariable int id,@RequestBody ProductRequest r) {
		Product o = ProductMapper.FromUpdateProductRequest(r);

		o = productService.setUpdateState(id,o);

		return new ResponseEntity<ProductResponse>(ProductMapper.ToUpdateProductResponse(o),HttpStatus.OK);
	}	

}