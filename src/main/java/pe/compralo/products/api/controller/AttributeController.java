package pe.compralo.products.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.compralo.products.api.response.PaginationResponse;
import pe.compralo.products.api.query.AttributeQuery;
import pe.compralo.products.api.request.AttributeRequest;
import pe.compralo.products.api.response.AttributeResponse;
import pe.compralo.products.application.domain.Attribute;
import pe.compralo.products.application.mapper.AttributeMapper;
import pe.compralo.products.service.interfaces.AttributeService;

@RestController
@RequestMapping("/attributes")
public class AttributeController {

	@Autowired
	private AttributeService attributeService;

	@GetMapping("/{id}")
	public ResponseEntity<AttributeResponse> getSelect(@PathVariable int id) {
		Attribute o = attributeService.getSelect(id);

		if(o==null) return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);

		return new ResponseEntity<AttributeResponse>(AttributeMapper.ToSelectAttributeResponse(o),HttpStatus.OK);
	}

	@GetMapping("/search")
	public ResponseEntity<List<AttributeResponse>> getSelect(AttributeQuery q) {
		List<Attribute> l = attributeService.getSelect(q.getId(),q.getPagination());

		return new ResponseEntity<List<AttributeResponse>>(AttributeMapper.ToArraySelectAttributeResponse(l),HttpStatus.OK);
	}

	@GetMapping("/find-all")
	public ResponseEntity<PaginationResponse<AttributeResponse>> getList(AttributeQuery q) {
		List<Attribute> l = attributeService.getList(q.getId(),q.getPagination());

		return new ResponseEntity<PaginationResponse<AttributeResponse>>(new PaginationResponse<AttributeResponse>(q.getPagination(),AttributeMapper.ToArrayListAttributeResponse(l)),HttpStatus.OK);
	}
	
	@GetMapping("/find-all/tenant")
	public ResponseEntity<List<AttributeResponse>> getListByTenant(AttributeQuery q) {
		List<Attribute> l = attributeService.getListByTenantWithItems(q.getTenantId(),q.getActive(),q.getPagination());

		return new ResponseEntity<List<AttributeResponse>>(AttributeMapper.ToArrayListWithItemsAttributeResponse(l),HttpStatus.OK);
	}	

	@PostMapping()
	public ResponseEntity<AttributeResponse> setRegister(@RequestBody AttributeRequest r) {
		Attribute o = AttributeMapper.FromAttributeRequest(r);

		attributeService.setRegister(o);

		return new ResponseEntity<AttributeResponse>(AttributeMapper.ToRegisterAttributeResponse(o),HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<AttributeResponse> setUpdate(@PathVariable int id,@RequestBody AttributeRequest r) {
		Attribute o = AttributeMapper.FromAttributeRequest(r);

		attributeService.setUpdate(id,o);

		return new ResponseEntity<AttributeResponse>(AttributeMapper.ToRegisterAttributeResponse(o),HttpStatus.OK);
	}

}