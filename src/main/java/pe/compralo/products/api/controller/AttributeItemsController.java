package pe.compralo.products.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.compralo.products.api.response.PaginationResponse;
import pe.compralo.products.api.query.AttributeItemsQuery;
import pe.compralo.products.api.request.AttributeItemsRequest;
import pe.compralo.products.api.response.AttributeItemsResponse;
import pe.compralo.products.application.domain.AttributeItems;
import pe.compralo.products.application.mapper.AttributeItemsMapper;
import pe.compralo.products.service.interfaces.AttributeItemsService;

@RestController
@RequestMapping("/attributeItemss")
public class AttributeItemsController {

	@Autowired
	private AttributeItemsService attributeItemsService;

	@GetMapping("/{id}")
	public ResponseEntity<AttributeItemsResponse> getSelect(@PathVariable int id) {
		AttributeItems o = attributeItemsService.getSelect(id);

		if(o==null) return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);

		return new ResponseEntity<AttributeItemsResponse>(AttributeItemsMapper.ToSelectAttributeItemsResponse(o),HttpStatus.OK);
	}

	@GetMapping("/search")
	public ResponseEntity<List<AttributeItemsResponse>> getSelect(AttributeItemsQuery q) {
		List<AttributeItems> l = attributeItemsService.getSelect(q.getId(),q.getPagination());

		return new ResponseEntity<List<AttributeItemsResponse>>(AttributeItemsMapper.ToArraySelectAttributeItemsResponse(l),HttpStatus.OK);
	}

	@GetMapping("/find-all")
	public ResponseEntity<PaginationResponse<AttributeItemsResponse>> getList(AttributeItemsQuery q) {
		List<AttributeItems> l = attributeItemsService.getList(q.getId(),q.getPagination());

		return new ResponseEntity<PaginationResponse<AttributeItemsResponse>>(new PaginationResponse<AttributeItemsResponse>(q.getPagination(),AttributeItemsMapper.ToArrayListAttributeItemsResponse(l)),HttpStatus.OK);
	}
	
	@GetMapping("/find-all/tenant")
	public ResponseEntity<List<AttributeItemsResponse>> getListByTenant(AttributeItemsQuery q) {
		List<AttributeItems> l = attributeItemsService.getListByTenant(q.getTenantId(),q.getPagination());

		return new ResponseEntity<List<AttributeItemsResponse>>(AttributeItemsMapper.ToArrayListAttributeItemsResponse(l),HttpStatus.OK);
	}

	@PostMapping()
	public ResponseEntity<AttributeItemsResponse> setRegister(@RequestBody AttributeItemsRequest r) {
		AttributeItems o = AttributeItemsMapper.FromAttributeItemsRequest(r);

		attributeItemsService.setRegister(o);

		return new ResponseEntity<AttributeItemsResponse>(AttributeItemsMapper.ToRegisterAttributeItemsResponse(o),HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<AttributeItemsResponse> setUpdate(@PathVariable int id,@RequestBody AttributeItemsRequest r) {
		AttributeItems o = AttributeItemsMapper.FromAttributeItemsRequest(r);

		attributeItemsService.setUpdate(id,o);

		return new ResponseEntity<AttributeItemsResponse>(AttributeItemsMapper.ToRegisterAttributeItemsResponse(o),HttpStatus.OK);
	}

}