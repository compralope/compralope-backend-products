package pe.compralo.products.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.compralo.products.api.response.PaginationResponse;
import pe.compralo.products.api.query.VariantQuery;
import pe.compralo.products.api.request.VariantRequest;
import pe.compralo.products.api.response.VariantResponse;
import pe.compralo.products.application.domain.Variant;
import pe.compralo.products.application.mapper.VariantMapper;
import pe.compralo.products.service.interfaces.VariantService;

@RestController
@RequestMapping("/variants")
public class VariantController {

	@Autowired
	private VariantService variantService;

	@GetMapping("/{id}")
	public ResponseEntity<VariantResponse> getSelect(@PathVariable int id) {
		Variant o = variantService.getSelect(id);

		if(o==null) return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);

		return new ResponseEntity<VariantResponse>(VariantMapper.ToSelectVariantResponse(o),HttpStatus.OK);
	}

	@GetMapping("/search")
	public ResponseEntity<List<VariantResponse>> getSelect(VariantQuery q) {
		List<Variant> l = variantService.getSelect(q.getId(),q.getPagination());

		return new ResponseEntity<List<VariantResponse>>(VariantMapper.ToArraySelectVariantResponse(l),HttpStatus.OK);
	}

	@GetMapping("/find-all")
	public ResponseEntity<PaginationResponse<VariantResponse>> getList(VariantQuery q) {
		List<Variant> l = variantService.getList(q.getId(),q.getPagination());

		return new ResponseEntity<PaginationResponse<VariantResponse>>(new PaginationResponse<VariantResponse>(q.getPagination(),VariantMapper.ToArrayListVariantResponse(l)),HttpStatus.OK);
	}

	@PostMapping()
	public ResponseEntity<VariantResponse> setRegister(@RequestBody VariantRequest r) {
		Variant o = VariantMapper.FromVariantRequest(r);

		variantService.setRegister(o);

		return new ResponseEntity<VariantResponse>(VariantMapper.ToRegisterVariantResponse(o),HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<VariantResponse> setUpdate(@PathVariable int id,@RequestBody VariantRequest r) {
		Variant o = VariantMapper.FromVariantRequest(r);

		variantService.setUpdate(id,o);

		return new ResponseEntity<VariantResponse>(VariantMapper.ToRegisterVariantResponse(o),HttpStatus.OK);
	}

}