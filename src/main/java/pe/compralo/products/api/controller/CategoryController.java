package pe.compralo.products.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.compralo.products.api.response.PaginationResponse;
import pe.compralo.products.api.query.CategoryQuery;
import pe.compralo.products.api.request.CategoryRequest;
import pe.compralo.products.api.response.CategoryResponse;
import pe.compralo.products.application.domain.Category;
import pe.compralo.products.application.mapper.CategoryMapper;
import pe.compralo.products.service.interfaces.CategoryService;

@RestController
@RequestMapping("/categorys")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@GetMapping("/{id}")
	public ResponseEntity<CategoryResponse> getSelect(@PathVariable int id) {
		Category o = categoryService.getSelect(id);

		if(o==null) return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);

		return new ResponseEntity<CategoryResponse>(CategoryMapper.ToSelectCategoryResponse(o),HttpStatus.OK);
	}

	@GetMapping("/search")
	public ResponseEntity<List<CategoryResponse>> getSelect(CategoryQuery q) {
		List<Category> l = categoryService.getSelect(q.getId(),q.getPagination());

		return new ResponseEntity<List<CategoryResponse>>(CategoryMapper.ToArraySelectCategoryResponse(l),HttpStatus.OK);
	}

	@GetMapping("/find-all")
	public ResponseEntity<PaginationResponse<CategoryResponse>> getList(CategoryQuery q) {
		List<Category> l = categoryService.getList(q.getId(),q.getPagination());

		return new ResponseEntity<PaginationResponse<CategoryResponse>>(new PaginationResponse<CategoryResponse>(q.getPagination(),CategoryMapper.ToArrayListCategoryResponse(l)),HttpStatus.OK);
	}
	
	@GetMapping("/find-all/tenant")
	public ResponseEntity<List<CategoryResponse>> getListByTenant(CategoryQuery q) {
		List<Category> l = categoryService.getListByTenant(q.getTenantId(),q.getActive(),q.getPagination());

		return new ResponseEntity<List<CategoryResponse>>(CategoryMapper.ToArrayListCategoryResponse(l),HttpStatus.OK);
	}	

	@PostMapping()
	public ResponseEntity<CategoryResponse> setRegister(@RequestBody CategoryRequest r) {
		Category o = CategoryMapper.FromCategoryRequest(r);

		categoryService.setRegister(o);

		return new ResponseEntity<CategoryResponse>(CategoryMapper.ToRegisterCategoryResponse(o),HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<CategoryResponse> setUpdate(@PathVariable int id,@RequestBody CategoryRequest r) {
		Category o = CategoryMapper.FromCategoryRequest(r);

		categoryService.setUpdate(id,o);

		return new ResponseEntity<CategoryResponse>(CategoryMapper.ToRegisterCategoryResponse(o),HttpStatus.OK);
	}

}