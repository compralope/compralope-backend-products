package pe.compralo.products.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.compralo.products.api.response.PaginationResponse;
import pe.compralo.products.api.query.VariantDetailQuery;
import pe.compralo.products.api.request.VariantDetailRequest;
import pe.compralo.products.api.response.VariantDetailResponse;
import pe.compralo.products.application.domain.VariantDetail;
import pe.compralo.products.application.mapper.VariantDetailMapper;
import pe.compralo.products.service.interfaces.VariantDetailService;

@RestController
@RequestMapping("/variantDetails")
public class VariantDetailController {

	@Autowired
	private VariantDetailService variantDetailService;

	@GetMapping("/{id}")
	public ResponseEntity<VariantDetailResponse> getSelect(@PathVariable int id) {
		VariantDetail o = variantDetailService.getSelect(id);

		if(o==null) return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);

		return new ResponseEntity<VariantDetailResponse>(VariantDetailMapper.ToSelectVariantDetailResponse(o),HttpStatus.OK);
	}

	@GetMapping("/search")
	public ResponseEntity<List<VariantDetailResponse>> getSelect(VariantDetailQuery q) {
		List<VariantDetail> l = variantDetailService.getSelect(q.getId(),q.getPagination());

		return new ResponseEntity<List<VariantDetailResponse>>(VariantDetailMapper.ToArraySelectVariantDetailResponse(l),HttpStatus.OK);
	}

	@GetMapping("/find-all")
	public ResponseEntity<PaginationResponse<VariantDetailResponse>> getList(VariantDetailQuery q) {
		List<VariantDetail> l = variantDetailService.getList(q.getId(),q.getPagination());

		return new ResponseEntity<PaginationResponse<VariantDetailResponse>>(new PaginationResponse<VariantDetailResponse>(q.getPagination(),VariantDetailMapper.ToArrayListVariantDetailResponse(l)),HttpStatus.OK);
	}

	@PostMapping()
	public ResponseEntity<VariantDetailResponse> setRegister(@RequestBody VariantDetailRequest r) {
		VariantDetail o = VariantDetailMapper.FromVariantDetailRequest(r);

		variantDetailService.setRegister(o);

		return new ResponseEntity<VariantDetailResponse>(VariantDetailMapper.ToRegisterVariantDetailResponse(o),HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<VariantDetailResponse> setUpdate(@PathVariable int id,@RequestBody VariantDetailRequest r) {
		VariantDetail o = VariantDetailMapper.FromVariantDetailRequest(r);

		variantDetailService.setUpdate(id,o);

		return new ResponseEntity<VariantDetailResponse>(VariantDetailMapper.ToRegisterVariantDetailResponse(o),HttpStatus.OK);
	}

}