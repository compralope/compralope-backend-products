package pe.compralo.products.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.compralo.products.api.response.PaginationResponse;
import pe.compralo.products.api.query.ProductMediaQuery;
import pe.compralo.products.api.request.ProductMediaRequest;
import pe.compralo.products.api.response.ProductMediaResponse;
import pe.compralo.products.application.domain.ProductMedia;
import pe.compralo.products.application.mapper.ProductMediaMapper;
import pe.compralo.products.service.interfaces.ProductMediaService;

@RestController
@RequestMapping("/productMedias")
public class ProductMediaController {

	@Autowired
	private ProductMediaService productMediaService;

	@GetMapping("/{id}")
	public ResponseEntity<ProductMediaResponse> getSelect(@PathVariable int id) {
		ProductMedia o = productMediaService.getSelect(id);

		if(o==null) return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);

		return new ResponseEntity<ProductMediaResponse>(ProductMediaMapper.ToSelectProductMediaResponse(o),HttpStatus.OK);
	}

	@GetMapping("/search")
	public ResponseEntity<List<ProductMediaResponse>> getSelect(ProductMediaQuery q) {
		List<ProductMedia> l = productMediaService.getSelect(q.getId(),q.getPagination());

		return new ResponseEntity<List<ProductMediaResponse>>(ProductMediaMapper.ToArraySelectProductMediaResponse(l),HttpStatus.OK);
	}

	@GetMapping("/find-all")
	public ResponseEntity<PaginationResponse<ProductMediaResponse>> getList(ProductMediaQuery q) {
		List<ProductMedia> l = productMediaService.getList(q.getId(),q.getPagination());

		return new ResponseEntity<PaginationResponse<ProductMediaResponse>>(new PaginationResponse<ProductMediaResponse>(q.getPagination(),ProductMediaMapper.ToArrayListProductMediaResponse(l)),HttpStatus.OK);
	}

	@PostMapping()
	public ResponseEntity<ProductMediaResponse> setRegister(@RequestBody ProductMediaRequest r) {
		ProductMedia o = ProductMediaMapper.FromProductMediaRequest(r);

		productMediaService.setRegister(o);

		return new ResponseEntity<ProductMediaResponse>(ProductMediaMapper.ToRegisterProductMediaResponse(o),HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<ProductMediaResponse> setUpdate(@PathVariable int id,@RequestBody ProductMediaRequest r) {
		ProductMedia o = ProductMediaMapper.FromProductMediaRequest(r);

		productMediaService.setUpdate(id,o);

		return new ResponseEntity<ProductMediaResponse>(ProductMediaMapper.ToRegisterProductMediaResponse(o),HttpStatus.OK);
	}

}