package pe.compralo.products.api.request;

import java.io.Serializable;

@SuppressWarnings("serial")
public class VariantDetailRequest implements Serializable
{

	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	private int variantId;
	public int getVariantId() {
		return variantId;
	}
	public void setVariantId(int variantId) {
		this.variantId = variantId;
	}

	private int attributeId;
	public int getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(int attributeId) {
		this.attributeId = attributeId;
	}	
	
	private String attributeTitle;	
	public String getAttributeTitle() {
		return attributeTitle;
	}
	public void setAttributeTitle(String attributeTitle) {
		this.attributeTitle = attributeTitle;
	}	
	
	private int attributeItemsId;
	public int getAttributeItemsId() {
		return attributeItemsId;
	}
	public void setAttributeItemsId(int atrributeItemsId) {
		this.attributeItemsId = atrributeItemsId;
	}
	
	private String attributeItemsTitle;
	public String getAttributeItemsTitle() {
		return attributeItemsTitle;
	}
	public void setAttributeItemsTitle(String atrributeItemsTitle) {
		this.attributeItemsTitle = atrributeItemsTitle;
	}
	
	private Boolean active;
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}

}