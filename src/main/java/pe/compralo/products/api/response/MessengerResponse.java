package pe.compralo.products.api.response;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MessengerResponse implements Serializable
{
	public MessengerResponse()
	{
	}
	
	public MessengerResponse(int code, String message)
	{
		this.code = code;
		this.message = message;
	}	
	
	private int code;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}	
	
	private String message;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
