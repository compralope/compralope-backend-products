package pe.compralo.products.api.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pe.compralo.products.application.domain.ProductMedia;

@SuppressWarnings("serial")
public class ProductResponse implements Serializable
{

	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	private int tenantId;
	public int getTenantId() {
		return tenantId;
	}
	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	private int stateId;
	public int getStateId() {
		return stateId;
	}
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}
	
	private String stateTitle;
	public String getStateTitle() {
		return stateTitle;
	}
	public void setStateTitle(String stateTitle) {
		this.stateTitle = stateTitle;
	}

	private String urlImage;
	public String getUrlImage() {
		return urlImage;
	}
	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}
	
	private String urlThumbnail;
	public String getUrlThumbnail() {
		return urlThumbnail;
	}
	public void setUrlThumbnail(String urlThumbnail) {
		this.urlThumbnail = urlThumbnail;
	}	

	private int categoryId;
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	
	private String categoryTitle;
	public String getCategoryTitle() {
		return categoryTitle;
	}
	public void setCategoryTitle(String categoryTitle) {
		this.categoryTitle = categoryTitle;
	}

	private String code;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	private String title;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	private String details;
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}

	private int currencyId;
	public int getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}

	private String currencyTitle;
	public String getCurrencyTitle() {
		return currencyTitle;
	}
	public void setCurrencyTitle(String currencyTitle) {
		this.currencyTitle = currencyTitle;
	}	
	
	private String sellingPriceTitle;
	public String getSellingPriceTitle() {
		return sellingPriceTitle;
	}
	public void setSellingPriceTitle(String sellingPriceTitle) {
		this.sellingPriceTitle = sellingPriceTitle;
	}

	private String url;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	private Boolean enableIgv;
	public Boolean getEnableIgv() {
		return enableIgv;
	}
	public void setEnableIgv(Boolean enableIgv) {
		this.enableIgv = enableIgv;
	}

	private Boolean enableVariant;
	public Boolean getEnableVariant() {
		return enableVariant;
	}
	public void setEnableVariant(Boolean enableVariant) {
		this.enableVariant = enableVariant;
	}
	
	private Double sellingPrice;
	public Double getSellingPrice() {
		return sellingPrice;
	}
	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	private Double costPrice;
	public Double getCostPrice() {
		return costPrice;
	}
	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	private Double costAverage;
	public Double getCostAverage() {
		return costAverage;
	}
	public void setCostAverage(Double costAverage) {
		this.costAverage = costAverage;
	}	
	
	private Double stock;
	public Double getStock() {
		return stock;
	}
	public void setStock(Double stock) {
		this.stock = stock;
	}
	

	private Boolean active;
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}

	private Date registerDate;
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	private Date updateDate;
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	private List<VariantResponse> variants = new ArrayList<VariantResponse>();
	public List<VariantResponse> getVariants() {
		return variants;
	}
	public void setVariants(List<VariantResponse> variants) {
		this.variants = variants;
	}	

	private List<ProductMediaResponse> images = new ArrayList<ProductMediaResponse>();
	public List<ProductMediaResponse> getImages() {
		return images;
	}
	public void setImages(List<ProductMediaResponse> images) {
		this.images = images;
	}
}