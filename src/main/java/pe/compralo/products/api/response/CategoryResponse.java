package pe.compralo.products.api.response;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CategoryResponse implements Serializable
{

	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	private int tenantId;
	public int getTenantId() {
		return tenantId;
	}
	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	private int categoryParentId;
	public int getCategoryParentId() {
		return categoryParentId;
	}
	public void setCategoryParentId(int categoryParentId) {
		this.categoryParentId = categoryParentId;
	}

	private String title;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	private Boolean active;
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}

}