package pe.compralo.products.api.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class AttributeResponse implements Serializable
{

	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	private String title;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	private Boolean active;
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}

	private List<AttributeItemsResponse> items = new ArrayList<AttributeItemsResponse>();
	public List<AttributeItemsResponse> getItems() {
		return items;
	}
	public void setItems(List<AttributeItemsResponse> items) {
		this.items = items;
	}	
}