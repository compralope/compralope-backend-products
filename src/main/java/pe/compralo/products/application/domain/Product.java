package pe.compralo.products.application.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Product
{

	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	private int tenantId;
	public int getTenantId() {
		return tenantId;
	}
	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	private int stateId;
	public int getStateId() {
		return stateId;
	}
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}
	
	private String stateTitle;
	public String getStateTitle() {
		return stateTitle;
	}
	public void setStateTitle(String stateTitle) {
		this.stateTitle = stateTitle;
	}

	private int categoryId;
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	
	private String categoryTitle;
	public String getCategoryTitle() {
		return categoryTitle;
	}
	public void setCategoryTitle(String categoryTitle) {
		this.categoryTitle = categoryTitle;
	}

	private String code;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	private String title;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	private String urlImage;
	public String getUrlImage() {
		return urlImage;
	}
	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}
	
	private String urlThumbnail;
	public String getUrlThumbnail() {
		return urlThumbnail;
	}
	public void setUrlThumbnail(String urlThumbnail) {
		this.urlThumbnail = urlThumbnail;
	}

	private String details;
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	
	private Double stock;
	public Double getStock() {
		return stock;
	}
	public void setStock(Double stock) {
		this.stock = stock;
	}

	private int currencyId;
	public int getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}

	private String currencyTitle;
	public String getCurrencyTitle() {
		return currencyTitle;
	}
	public void setCurrencyTitle(String currencyTitle) {
		this.currencyTitle = currencyTitle;
	}

	private Double sellingPriceMax;
	public Double getSellingPriceMax() {
		return sellingPriceMax;
	}
	public void setSellingPriceMax(Double sellingPriceMax) {
		this.sellingPriceMax = sellingPriceMax;
	}

	private Double sellingPriceMin;
	public Double getSellingPriceMin() {
		return sellingPriceMin;
	}
	public void setSellingPriceMin(Double sellingPriceMin) {
		this.sellingPriceMin = sellingPriceMin;
	}

	private String url;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	private Boolean enableIgv;
	public Boolean getEnableIgv() {
		return enableIgv;
	}
	public void setEnableIgv(Boolean enableIgv) {
		this.enableIgv = enableIgv;
	}

	private Boolean enableVariant;
	public Boolean getEnableVariant() {
		return enableVariant;
	}
	public void setEnableVariant(Boolean enableVariant) {
		this.enableVariant = enableVariant;
	}

	private Boolean active;
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}

	private Date registerDate;
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	private Date updateDate;
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	private List<Variant> variants = new ArrayList<Variant>();;
	public List<Variant> getVariants() {
		return variants;
	}
	public void setVariants(List<Variant> variants) {
		this.variants = variants;
	}
	
	private List<ProductMedia> images = new ArrayList<ProductMedia>();
	public List<ProductMedia> getImages() {
		return images;
	}
	public void setImages(List<ProductMedia> images) {
		this.images = images;
	}


}