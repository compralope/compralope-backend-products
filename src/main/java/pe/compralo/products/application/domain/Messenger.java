package pe.compralo.products.application.domain;

public class Messenger {
	
	public Messenger()
	{
	}
	
	public Messenger(int code, String message)
	{
		this.code = code;
		this.message = message;
	}	
	
	private int code;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}	
	
	private String message;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
