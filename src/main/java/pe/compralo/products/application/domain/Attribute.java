package pe.compralo.products.application.domain;

import java.util.ArrayList;
import java.util.List;

public class Attribute
{
	public Attribute()
	{
	}
	
	public Attribute(String title,Boolean active)
	{
		this.title=title;
		this.active=active;
	}	

	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	private String title;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	private Boolean active;
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	private List<AttributeItems> items = new ArrayList<AttributeItems>();
	public List<AttributeItems> getItems() {
		return items;
	}
	public void setItems(List<AttributeItems> items) {
		this.items = items;
	}
	

}