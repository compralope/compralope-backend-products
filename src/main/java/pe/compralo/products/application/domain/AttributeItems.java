package pe.compralo.products.application.domain;

public class AttributeItems
{

	public AttributeItems()
	{
	}
	
	public AttributeItems(int attributeId,String title,Boolean active)
	{
		this.attributeId = attributeId;
		this.title = title;
		this.active = active;
	}
	
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	private int attributeId;
	public int getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(int attributeId) {
		this.attributeId = attributeId;
	}

	private String title;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	private Boolean active;
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}

}