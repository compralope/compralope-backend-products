package pe.compralo.products.application.domain;

import java.util.ArrayList;
import java.util.List;

public class Variant
{

	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	private int productId;
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}

	private int stateId;
	public int getStateId() {
		return stateId;
	}
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	private String code;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}	
	
	private String title;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	private Double sellingPrice;
	public Double getSellingPrice() {
		return sellingPrice;
	}
	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	private Double costPrice;
	public Double getCostPrice() {
		return costPrice;
	}
	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	private Double costAverage;
	public Double getCostAverage() {
		return costAverage;
	}
	public void setCostAverage(Double costAverage) {
		this.costAverage = costAverage;
	}

	private Double stock;
	public Double getStock() {
		return stock;
	}
	public void setStock(Double stock) {
		this.stock = stock;
	}

	private Boolean active;
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	private List<VariantDetail> details = new ArrayList<VariantDetail>();
	public List<VariantDetail> getDetails() {
		return details;
	}
	public void setDetails(List<VariantDetail> details) {
		this.details = details;
	}
}