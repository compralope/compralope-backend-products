package pe.compralo.products.application.mapper;

import java.util.ArrayList;
import java.util.List;

import pe.compralo.products.api.request.AttributeRequest;
import pe.compralo.products.api.response.AttributeResponse;
import pe.compralo.products.application.domain.Attribute;
import pe.compralo.products.application.domain.AttributeItems;

public class AttributeMapper {

	public static AttributeResponse ToSelectAttributeResponse(Attribute i) {
		AttributeResponse o = new AttributeResponse();

		o.setId(i.getId());
		o.setTitle(i.getTitle());
		o.setActive(i.getActive());

		return o;
	}

	public static List<AttributeResponse> ToArraySelectAttributeResponse(List<Attribute> l) {
		List<AttributeResponse> o = new ArrayList<AttributeResponse>();

		for(Attribute i:l)
			o.add(AttributeMapper.ToSelectAttributeResponse(i));

		return o;
	}

	public static AttributeResponse ToListAttributeResponse(Attribute i) {
		AttributeResponse o = new AttributeResponse();

		o.setId(i.getId());
		o.setTitle(i.getTitle());
		o.setActive(i.getActive());

		return o;
	}

	public static List<AttributeResponse> ToArrayListAttributeResponse(List<Attribute> l) {
		List<AttributeResponse> o = new ArrayList<AttributeResponse>();

		for(Attribute i:l)
			o.add(AttributeMapper.ToListAttributeResponse(i));

		return o;
	}
	
	public static AttributeResponse ToListWithItemsAttributeResponse(Attribute i) {
		AttributeResponse o = new AttributeResponse();

		o.setId(i.getId());
		o.setTitle(i.getTitle());
		o.setActive(i.getActive());
		
		for(AttributeItems item:i.getItems())
			o.getItems().add(AttributeItemsMapper.ToListAttributeItemsResponse(item));

		return o;
	}

	public static List<AttributeResponse> ToArrayListWithItemsAttributeResponse(List<Attribute> l) {
		List<AttributeResponse> o = new ArrayList<AttributeResponse>();

		for(Attribute i:l)
			o.add(AttributeMapper.ToListWithItemsAttributeResponse(i));

		return o;
	}	

	public static Attribute FromAttributeRequest(AttributeRequest i) {
		Attribute o = new Attribute();

		o.setId(i.getId());
		o.setTitle(i.getTitle());
		o.setActive(i.getActive());

		return o;
	}

	public static AttributeResponse ToRegisterAttributeResponse(Attribute i) {
		AttributeResponse o = new AttributeResponse();

		o.setId(i.getId());
		o.setTitle(i.getTitle());
		o.setActive(i.getActive());

		return o;
	}

}