package pe.compralo.products.application.mapper;

import java.util.ArrayList;
import java.util.List;

import pe.compralo.products.api.request.AttributeItemsRequest;
import pe.compralo.products.api.response.AttributeItemsResponse;
import pe.compralo.products.application.domain.AttributeItems;

public class AttributeItemsMapper {

	public static AttributeItemsResponse ToSelectAttributeItemsResponse(AttributeItems i) {
		AttributeItemsResponse o = new AttributeItemsResponse();

		o.setId(i.getId());
		o.setAttributeId(i.getAttributeId());
		o.setTitle(i.getTitle());
		o.setActive(i.getActive());

		return o;
	}

	public static List<AttributeItemsResponse> ToArraySelectAttributeItemsResponse(List<AttributeItems> l) {
		List<AttributeItemsResponse> o = new ArrayList<AttributeItemsResponse>();

		for(AttributeItems i:l)
			o.add(AttributeItemsMapper.ToSelectAttributeItemsResponse(i));

		return o;
	}

	public static AttributeItemsResponse ToListAttributeItemsResponse(AttributeItems i) {
		AttributeItemsResponse o = new AttributeItemsResponse();

		o.setId(i.getId());
		o.setAttributeId(i.getAttributeId());
		o.setTitle(i.getTitle());
		o.setActive(i.getActive());

		return o;
	}

	public static List<AttributeItemsResponse> ToArrayListAttributeItemsResponse(List<AttributeItems> l) {
		List<AttributeItemsResponse> o = new ArrayList<AttributeItemsResponse>();

		for(AttributeItems i:l)
			o.add(AttributeItemsMapper.ToListAttributeItemsResponse(i));

		return o;
	}

	public static AttributeItems FromAttributeItemsRequest(AttributeItemsRequest i) {
		AttributeItems o = new AttributeItems();

		o.setId(i.getId());
		o.setAttributeId(i.getAttributeId());
		o.setTitle(i.getTitle());
		o.setActive(i.getActive());

		return o;
	}

	public static AttributeItemsResponse ToRegisterAttributeItemsResponse(AttributeItems i) {
		AttributeItemsResponse o = new AttributeItemsResponse();

		o.setId(i.getId());
		o.setAttributeId(i.getAttributeId());
		o.setTitle(i.getTitle());
		o.setActive(i.getActive());

		return o;
	}

}