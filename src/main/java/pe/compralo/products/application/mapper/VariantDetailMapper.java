package pe.compralo.products.application.mapper;

import java.util.ArrayList;
import java.util.List;

import pe.compralo.products.api.request.VariantDetailRequest;
import pe.compralo.products.api.response.VariantDetailResponse;
import pe.compralo.products.application.domain.VariantDetail;

public class VariantDetailMapper {

	public static VariantDetailResponse ToSelectVariantDetailResponse(VariantDetail i) {
		VariantDetailResponse o = new VariantDetailResponse();

		o.setId(i.getId());
		o.setVariantId(i.getVariantId());
		o.setAttributeId(i.getAttributeId());
		o.setAttributeTitle(i.getAttributeTitle());		
		o.setAttributeItemsId(i.getAttributeItemsId());
		o.setAttributeItemsTitle(i.getAttributeItemsTitle());
		o.setActive(i.getActive());

		return o;
	}

	public static List<VariantDetailResponse> ToArraySelectVariantDetailResponse(List<VariantDetail> l) {
		List<VariantDetailResponse> o = new ArrayList<VariantDetailResponse>();

		for(VariantDetail i:l)
			o.add(VariantDetailMapper.ToSelectVariantDetailResponse(i));

		return o;
	}

	public static VariantDetailResponse ToListVariantDetailResponse(VariantDetail i) {
		VariantDetailResponse o = new VariantDetailResponse();

		o.setId(i.getId());
		o.setVariantId(i.getVariantId());
		o.setAttributeItemsId(i.getAttributeItemsId());

		return o;
	}

	public static List<VariantDetailResponse> ToArrayListVariantDetailResponse(List<VariantDetail> l) {
		List<VariantDetailResponse> o = new ArrayList<VariantDetailResponse>();

		for(VariantDetail i:l)
			o.add(VariantDetailMapper.ToListVariantDetailResponse(i));

		return o;
	}

	public static VariantDetail FromVariantDetailRequest(VariantDetailRequest i) {
		VariantDetail o = new VariantDetail();

		o.setId(i.getId());
		o.setVariantId(i.getVariantId());
		o.setAttributeTitle(i.getAttributeTitle());
		o.setAttributeItemsId(i.getAttributeItemsId());
		o.setAttributeItemsTitle(i.getAttributeItemsTitle());
		o.setActive(i.getActive());
		
		return o;
	}

	public static VariantDetailResponse ToRegisterVariantDetailResponse(VariantDetail i) {
		VariantDetailResponse o = new VariantDetailResponse();

		o.setId(i.getId());
		o.setVariantId(i.getVariantId());
		o.setAttributeId(i.getAttributeId());
		o.setAttributeTitle(i.getAttributeTitle());		
		o.setAttributeItemsId(i.getAttributeItemsId());
		o.setAttributeItemsTitle(i.getAttributeItemsTitle());
		o.setActive(i.getActive());
		
		return o;
	}

}