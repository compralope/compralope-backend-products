package pe.compralo.products.application.mapper;

import java.util.ArrayList;
import java.util.List;

import pe.compralo.products.api.request.VariantDetailRequest;
import pe.compralo.products.api.request.VariantRequest;
import pe.compralo.products.api.response.VariantResponse;
import pe.compralo.products.application.domain.Variant;

public class VariantMapper {

	public static VariantResponse ToSelectVariantResponse(Variant i) {
		VariantResponse o = new VariantResponse();

		o.setId(i.getId());
		o.setProductId(i.getProductId());
		o.setCode(i.getCode());
		o.setStateId(i.getStateId());
		o.setTitle(i.getTitle());
		o.setSellingPrice(i.getSellingPrice());
		o.setCostPrice(i.getCostPrice());
		o.setCostAverage(i.getCostAverage());
		o.setStock(i.getStock());
		o.setActive(i.getActive());
		
		return o;
	}

	public static List<VariantResponse> ToArraySelectVariantResponse(List<Variant> l) {
		List<VariantResponse> o = new ArrayList<VariantResponse>();

		for(Variant i:l)
			o.add(VariantMapper.ToSelectVariantResponse(i));

		return o;
	}

	public static VariantResponse ToListVariantResponse(Variant i) {
		VariantResponse o = new VariantResponse();

		o.setId(i.getId());
		o.setProductId(i.getProductId());
		o.setStateId(i.getStateId());
		o.setTitle(i.getTitle());
		o.setSellingPrice(i.getSellingPrice());
		o.setCostPrice(i.getCostPrice());
		o.setCostAverage(i.getCostAverage());
		o.setStock(i.getStock());
		o.setActive(i.getActive());

		return o;
	}

	public static List<VariantResponse> ToArrayListVariantResponse(List<Variant> l) {
		List<VariantResponse> o = new ArrayList<VariantResponse>();

		for(Variant i:l)
			o.add(VariantMapper.ToListVariantResponse(i));

		return o;
	}

	public static Variant FromVariantRequest(VariantRequest i) {
		Variant o = new Variant();

		o.setId(i.getId());
		o.setProductId(i.getProductId());
		o.setStateId(i.getStateId());
		o.setCode(i.getCode());
		o.setTitle(i.getTitle());
		o.setSellingPrice(i.getSellingPrice());
		o.setCostPrice(i.getCostPrice());
		o.setCostAverage(i.getCostAverage());
		o.setStock(i.getStock());
		o.setActive(i.getActive());

		for(VariantDetailRequest vd:i.getDetails())
			o.getDetails().add(VariantDetailMapper.FromVariantDetailRequest(vd));
		
		return o;
	}

	public static VariantResponse ToRegisterVariantResponse(Variant i) {
		VariantResponse o = new VariantResponse();

		o.setId(i.getId());
		o.setProductId(i.getProductId());
		o.setStateId(i.getStateId());
		o.setCode(i.getCode());
		o.setTitle(i.getTitle());
		o.setSellingPrice(i.getSellingPrice());
		o.setCostPrice(i.getCostPrice());
		o.setCostAverage(i.getCostAverage());
		o.setStock(i.getStock());
		o.setActive(i.getActive());

		return o;
	}

}