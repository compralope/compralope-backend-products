package pe.compralo.products.application.mapper;

import java.util.ArrayList;
import java.util.List;

import pe.compralo.products.api.request.CategoryRequest;
import pe.compralo.products.api.response.CategoryResponse;
import pe.compralo.products.application.domain.Category;

public class CategoryMapper {

	public static CategoryResponse ToSelectCategoryResponse(Category i) {
		CategoryResponse o = new CategoryResponse();

		o.setId(i.getId());
		o.setTenantId(i.getTenantId());
		o.setCategoryParentId(i.getCategoryParentId());
		o.setTitle(i.getTitle());
		o.setActive(i.getActive());

		return o;
	}

	public static List<CategoryResponse> ToArraySelectCategoryResponse(List<Category> l) {
		List<CategoryResponse> o = new ArrayList<CategoryResponse>();

		for(Category i:l)
			o.add(CategoryMapper.ToSelectCategoryResponse(i));

		return o;
	}

	public static CategoryResponse ToListCategoryResponse(Category i) {
		CategoryResponse o = new CategoryResponse();

		o.setId(i.getId());
		o.setTenantId(i.getTenantId());
		o.setCategoryParentId(i.getCategoryParentId());
		o.setTitle(i.getTitle());
		o.setActive(i.getActive());

		return o;
	}

	public static List<CategoryResponse> ToArrayListCategoryResponse(List<Category> l) {
		List<CategoryResponse> o = new ArrayList<CategoryResponse>();

		for(Category i:l)
			o.add(CategoryMapper.ToListCategoryResponse(i));

		return o;
	}

	public static Category FromCategoryRequest(CategoryRequest i) {
		Category o = new Category();

		o.setId(i.getId());
		o.setTenantId(i.getTenantId());
		o.setCategoryParentId(i.getCategoryParentId());
		o.setTitle(i.getTitle());
		o.setActive(i.getActive());

		return o;
	}

	public static CategoryResponse ToRegisterCategoryResponse(Category i) {
		CategoryResponse o = new CategoryResponse();

		o.setId(i.getId());
		o.setTenantId(i.getTenantId());
		o.setCategoryParentId(i.getCategoryParentId());
		o.setTitle(i.getTitle());
		o.setActive(i.getActive());

		return o;
	}

}