																																																																	package pe.compralo.products.application.mapper;

import java.util.ArrayList;
import java.util.List;

import pe.compralo.products.api.request.ProductMediaRequest;
import pe.compralo.products.api.response.ProductMediaResponse;
import pe.compralo.products.application.domain.ProductMedia;

public class ProductMediaMapper {

	public static ProductMediaResponse ToSelectProductMediaResponse(ProductMedia i) {
		ProductMediaResponse o = new ProductMediaResponse();

		o.setId(i.getId());
		o.setProductId(i.getProductId());
		o.setFileStorageId(i.getFileStorageId());
		o.setImageType(i.getImageType());
		o.setUrl(i.getUrl());
		o.setPrincipal(i.getPrincipal());
		o.setOrderIndex(i.getOrderIndex());
		o.setEnable(i.getEnable());

		return o;
	}

	public static List<ProductMediaResponse> ToArraySelectProductMediaResponse(List<ProductMedia> l) {
		List<ProductMediaResponse> o = new ArrayList<ProductMediaResponse>();

		for(ProductMedia i:l)
			o.add(ProductMediaMapper.ToSelectProductMediaResponse(i));

		return o;
	}

	public static ProductMediaResponse ToListProductMediaResponse(ProductMedia i) {
		ProductMediaResponse o = new ProductMediaResponse();

		o.setId(i.getId());
		o.setProductId(i.getProductId());
		o.setFileStorageId(i.getFileStorageId());
		o.setOrderIndex(i.getOrderIndex());
		o.setEnable(i.getEnable());

		return o;
	}

	public static List<ProductMediaResponse> ToArrayListProductMediaResponse(List<ProductMedia> l) {
		List<ProductMediaResponse> o = new ArrayList<ProductMediaResponse>();

		for(ProductMedia i:l)
			o.add(ProductMediaMapper.ToListProductMediaResponse(i));

		return o;
	}

	public static ProductMedia FromProductMediaRequest(ProductMediaRequest i) {
		ProductMedia o = new ProductMedia();

		o.setId(i.getId());
		o.setProductId(i.getProductId());		
		o.setFileStorageId(i.getFileStorageId());
		o.setImageType(i.getImageType());
		o.setUrl(i.getUrl());
		o.setOrderIndex(i.getOrderIndex());
		o.setPrincipal(i.getPrincipal());
		o.setEnable(i.getEnable());

		return o;
	}
	
	public static List<ProductMedia> FromArrayListProductMediaRequest(List<ProductMediaRequest> li) {
		List<ProductMedia> lo = new ArrayList<ProductMedia>();
		
		int count = 0;
		for(ProductMediaRequest i:li)
		{
			i.setOrderIndex(0);
			i.setPrincipal(false);
			
			if(i.getEnable())
			{
				count++;
				i.setOrderIndex(count);
				
				if(count<=2) i.setPrincipal(true);
			}
			
			lo.add(ProductMediaMapper.FromProductMediaRequest(i));
		}

		return lo;
	}	

	public static ProductMediaResponse ToRegisterProductMediaResponse(ProductMedia i) {
		ProductMediaResponse o = new ProductMediaResponse();

		o.setId(i.getId());
		o.setProductId(i.getProductId());
		o.setFileStorageId(i.getFileStorageId());
		o.setImageType(i.getImageType());
		o.setUrl(i.getUrl());
		o.setPrincipal(i.getPrincipal());
		o.setOrderIndex(i.getOrderIndex());
		o.setEnable(i.getEnable());

		return o;
	}

}