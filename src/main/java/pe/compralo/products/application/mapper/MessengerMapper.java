package pe.compralo.products.application.mapper;

import pe.compralo.products.api.response.MessengerResponse;
import pe.compralo.products.application.domain.Messenger;

public class MessengerMapper {
	public static MessengerResponse ToSelectMessageResponse(Messenger i) {
		MessengerResponse o = new MessengerResponse(i.getCode(),i.getMessage());
		
		return o;
	}
}
