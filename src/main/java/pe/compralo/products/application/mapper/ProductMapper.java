package pe.compralo.products.application.mapper;

import java.util.ArrayList;
import java.util.List;

import pe.compralo.products.api.request.ProductRequest;
import pe.compralo.products.api.request.VariantRequest;
import pe.compralo.products.api.response.ProductInboxResponse;
import pe.compralo.products.api.response.ProductResponse;
import pe.compralo.products.api.response.VariantResponse;
import pe.compralo.products.application.domain.Product;
import pe.compralo.products.application.domain.ProductMedia;
import pe.compralo.products.application.domain.Variant;
import pe.compralo.products.application.domain.VariantDetail;
import pe.compralo.products.cross.utils.ConvertFormat;

public class ProductMapper {

	public static ProductResponse ToSelectProductResponse(Product i) {
		ProductResponse o = new ProductResponse();

		o.setId(i.getId());
		o.setTenantId(i.getTenantId());
		o.setStateId(i.getStateId());
		o.setStateTitle(i.getStateTitle());
		o.setCategoryId(i.getCategoryId());
		o.setCategoryTitle(i.getCategoryTitle());
		o.setStock(i.getStock());
		o.setCurrencyId(i.getCurrencyId());
		o.setCurrencyTitle(i.getCurrencyTitle());
		o.setCode(i.getCode());
		o.setTitle(i.getTitle());
		o.setDetails(i.getDetails());
		o.setUrl(i.getUrl());
		o.setEnableIgv(i.getEnableIgv());
		o.setEnableVariant(i.getEnableVariant());
		o.setActive(i.getActive());
		o.setRegisterDate(i.getRegisterDate());
		o.setUpdateDate(i.getUpdateDate());

		if(o.getEnableVariant())
		{
			VariantResponse vr = null;
			for(Variant v:i.getVariants())
			{
				vr = VariantMapper.ToSelectVariantResponse(v);
				
				for(VariantDetail vd:v.getDetails())
					vr.getDetails().add(VariantDetailMapper.ToSelectVariantDetailResponse(vd));
				
				o.getVariants().add(vr);
			}			
		}
		else
		{
			Variant v = i.getVariants().get(0);
			
			o.setSellingPrice(v.getSellingPrice());
			o.setCostPrice(v.getCostPrice());
			o.setCostAverage(v.getCostAverage());
			o.setStock(v.getStock());
		}
		
		for(ProductMedia pm:i.getImages())
			o.getImages().add(ProductMediaMapper.ToSelectProductMediaResponse(pm));		

		return o;
	}

	public static List<ProductResponse> ToArraySelectProductResponse(List<Product> l) {
		List<ProductResponse> o = new ArrayList<ProductResponse>();

		for(Product i:l)
			o.add(ProductMapper.ToSelectProductResponse(i));

		return o;
	}

	public static ProductResponse ToListProductResponse(Product i) {
		ProductResponse o = new ProductResponse();

		o.setId(i.getId());
		o.setTenantId(i.getTenantId());
		o.setStateId(i.getStateId());
		o.setCategoryId(i.getCategoryId());
		o.setCode(i.getCode());
		o.setTitle(i.getTitle());
		o.setDetails(i.getDetails());
		o.setUrl(i.getUrl());
		o.setUrlImage(i.getUrlImage());
		o.setUrlThumbnail(i.getUrlThumbnail());		
		o.setEnableIgv(i.getEnableIgv());
		o.setEnableVariant(i.getEnableVariant());
		o.setActive(i.getActive());
		o.setRegisterDate(i.getRegisterDate());
		o.setUpdateDate(i.getUpdateDate());

		return o;
	}

	public static List<ProductResponse> ToArrayListProductResponse(List<Product> l) {
		List<ProductResponse> o = new ArrayList<ProductResponse>();

		for(Product i:l)
			o.add(ProductMapper.ToListProductResponse(i));

		return o;
	}

	public static ProductInboxResponse ToListByInboxProductResponse(Product i) {
		ProductInboxResponse o = new ProductInboxResponse();

		o.setId(i.getId());
		o.setTenantId(i.getTenantId());
		o.setStateId(i.getStateId());
		o.setStateTitle(i.getStateTitle());
		o.setCategoryId(i.getCategoryId());
		o.setCategoryTitle(i.getCategoryTitle());
		o.setStock(i.getStock());
		o.setCurrencyId(i.getCurrencyId());
		o.setCurrencyTitle(i.getCurrencyTitle());
		
		
		if(i.getSellingPriceMax().equals(i.getSellingPriceMin()))
			o.setSellingPriceTitle(o.getCurrencyTitle() + " " + ConvertFormat.DoubleToString(i.getSellingPriceMax()));
		else
			o.setSellingPriceTitle(o.getCurrencyTitle() + " " + ConvertFormat.DoubleToString(i.getSellingPriceMin()) + " - " + o.getCurrencyTitle() + " " + ConvertFormat.DoubleToString(i.getSellingPriceMax()));
		
		o.setCode(i.getCode());
		o.setTitle(i.getTitle());
		o.setUrl(i.getUrl());
		o.setUrlImage(i.getUrlImage());
		o.setUrlThumbnail(i.getUrlThumbnail());				
		o.setEnableIgv(i.getEnableIgv());
		o.setEnableVariant(i.getEnableVariant());
		o.setActive(i.getActive());

		return o;
	}

	public static List<ProductInboxResponse> ToArrayListByInboxProductResponse(List<Product> l) {
		List<ProductInboxResponse> o = new ArrayList<ProductInboxResponse>();

		for(Product i:l)
			o.add(ProductMapper.ToListByInboxProductResponse(i));

		return o;
	}
	
	public static Product FromUpdateProductRequest(ProductRequest i) {
		Product o = new Product();

		o.setId(i.getId());
		o.setTenantId(i.getTenantId());
		o.setStateId(i.getStateId());
		o.setCategoryId(i.getCategoryId());
		o.setCode(i.getCode());
		o.setTitle(i.getTitle());
		o.setDetails(i.getDetails());
		o.setUrl(i.getUrl());
		o.setEnableIgv(i.getEnableIgv());
		o.setEnableVariant(i.getEnableVariant());
		o.setActive(i.getActive());
		
		return o;
	}
	
	public static Product FromRegisterProductRequest(ProductRequest i) {
		Product o = new Product();

		o.setId(i.getId());
		o.setTenantId(i.getTenantId());
		o.setStateId(i.getStateId());
		o.setCategoryId(i.getCategoryId());
		o.setCategoryTitle(i.getCategoryTitle());
		o.setCurrencyId(i.getCurrencyId());
		o.setCode(i.getCode());
		o.setTitle(i.getTitle());
		o.setDetails(i.getDetails());
		o.setUrl(i.getUrl());
		o.setEnableIgv(i.getEnableIgv());
		o.setEnableVariant(i.getEnableVariant());
		o.setActive(i.getActive());

		//If don't have variants, create one for default 
		if(!o.getEnableVariant())
		{
			VariantRequest v = new VariantRequest();
			
			v.setTitle(o.getTitle());
			v.setCode(o.getCode());
			v.setSellingPrice(i.getSellingPrice());
			v.setCostPrice(i.getCostPrice());
			v.setCostAverage(i.getCostAverage());
			v.setStock(i.getStock());
			v.setActive(i.getActive());
			
			i.getVariants().add(v);
		}
		
		for(VariantRequest v:i.getVariants())
			o.getVariants().add(VariantMapper.FromVariantRequest(v));

		o.setImages(ProductMediaMapper.FromArrayListProductMediaRequest(i.getImages()));		
		
		return o;
	}

	public static ProductResponse ToRegisterProductResponse(Product i) {
		ProductResponse o = new ProductResponse();

		o.setId(i.getId());
		o.setTenantId(i.getTenantId());
		o.setStateId(i.getStateId());
		o.setCategoryId(i.getCategoryId());
		o.setCurrencyId(i.getCurrencyId());
		o.setCode(i.getCode());
		o.setTitle(i.getTitle());
		o.setDetails(i.getDetails());
		o.setUrl(i.getUrl());
		o.setEnableIgv(i.getEnableIgv());
		o.setEnableVariant(i.getEnableVariant());
		o.setActive(i.getActive());
		o.setRegisterDate(i.getRegisterDate());
		o.setUpdateDate(i.getUpdateDate());

		if(o.getEnableVariant())
		{
			VariantResponse vr = null;
			for(Variant v:i.getVariants())
			{
				vr = VariantMapper.ToRegisterVariantResponse(v);
				
				for(VariantDetail vd:v.getDetails())
					vr.getDetails().add(VariantDetailMapper.ToRegisterVariantDetailResponse(vd));
				
				o.getVariants().add(vr);
			}			
		}
		else
		{
			Variant v = i.getVariants().get(0);
			
			o.setSellingPrice(v.getSellingPrice());
			o.setCostPrice(v.getCostPrice());
			o.setCostAverage(v.getCostAverage());
			o.setStock(v.getStock());
		}

		for(ProductMedia pm:i.getImages())
			o.getImages().add(ProductMediaMapper.ToRegisterProductMediaResponse(pm));		
		
		return o;
	}

	public static ProductResponse ToUpdateProductResponse(Product i) {
		ProductResponse o = new ProductResponse();

		o.setId(i.getId());
		o.setTenantId(i.getTenantId());
		o.setStateId(i.getStateId());
		o.setCategoryId(i.getCategoryId());
		o.setCategoryTitle(i.getCategoryTitle());
		o.setCode(i.getCode());
		o.setTitle(i.getTitle());
		o.setDetails(i.getDetails());
		o.setUrl(i.getUrl());
		o.setEnableIgv(i.getEnableIgv());
		o.setEnableVariant(i.getEnableVariant());
		o.setActive(i.getActive());
		o.setRegisterDate(i.getRegisterDate());
		o.setUpdateDate(i.getUpdateDate());
		
		return o;
	}	
	
}