package pe.compralo.products.service.implementation;

import java.util.List;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.compralo.products.repository.interfaces.CategoryRepository;
import pe.compralo.products.service.interfaces.CategoryService;
import pe.compralo.products.application.domain.Category;
import pe.compralo.products.application.domain.Pagination;

@Service
public class CategoryServiceImp implements CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public Category getSelect(int id) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("id",id);

		return categoryRepository.getSearch(parameters);
	}

	@Override
	public List<Category> getSelect(int id, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		if(id > 0) parameters.put("id",id);

		return categoryRepository.getSearch(parameters, pagination);
	}
	
	@Override
	public Category getSelectByTenantTitle(int tenantId, String title) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("tenant_id",tenantId);
		parameters.put("title",title);

		return categoryRepository.getSearch(parameters);
	}	

	@Override
	public List<Category> getList(int id, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		if(id > 0) parameters.put("id",id);

		return categoryRepository.getFindAll(parameters, pagination);
	}

	@Override
	public List<Category> getListByTenant(int tenantId, Boolean active, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("tenant_id",tenantId);
		parameters.put("active",active);

		return categoryRepository.getFindAll(parameters, pagination);
	}	
	
	@Override
	public void setRegister(Category o) {
		categoryRepository.setInsertUpdate(o);
	}
	
	@Override
	public Category setCreate(int tenantId, String title) {
		Category category = this.getSelectByTenantTitle(tenantId, title);
		
		if(category == null)
		{
			category = new Category();
			
			category.setTenantId(tenantId);
			category.setTitle(title);
		}
		
		category.setActive(true);
		
		categoryRepository.setInsertUpdate(category);
		
		return category;
	}	

	@Override
	public void setUpdate(int id,Category o) {
		o.setId(id);

		categoryRepository.setInsertUpdate(o);
	}

}