package pe.compralo.products.service.implementation;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.compralo.products.repository.interfaces.ProductMediaRepository;
import pe.compralo.products.service.interfaces.ProductMediaService;
import pe.compralo.products.application.domain.ProductMedia;
import pe.compralo.products.application.domain.VariantDetail;
import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.application.domain.Product;

@Service
public class ProductMediaServiceImp implements ProductMediaService {

	@Autowired
	private ProductMediaRepository productMediaRepository;

	@Override
	public ProductMedia getSelect(int id) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("id",id);

		return productMediaRepository.getSearch(parameters);
	}

	@Override
	public List<ProductMedia> getSelect(int id, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		if(id > 0) parameters.put("id",id);

		return productMediaRepository.getSearch(parameters, pagination);
	}
	
	@Override
	public List<ProductMedia> getSelectByProduct(int productId, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("product_id",productId);

		return productMediaRepository.getSearch(parameters, pagination);
	}	
	
	@Override
	public List<ProductMedia> getSelectActiveByProduct(int productId) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("product_id",productId);
		parameters.put("enable",true);

		return productMediaRepository.getSearch(parameters, new Pagination("order_index ASC"));
	}		

	@Override
	public List<ProductMedia> getList(int id, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		if(id > 0) parameters.put("id",id);

		return productMediaRepository.getFindAll(parameters, pagination);
	}

	@Override
	public void setRegister(ProductMedia o) {
		productMediaRepository.setInsertUpdate(o);
	}

	@Override
	public void setUpdate(int id,ProductMedia o) {
		o.setId(id);

		productMediaRepository.setInsertUpdate(o);
	}
	
	@Override
	public void setRegisterMassive(Product product) {

		for(ProductMedia pm:product.getImages())
			pm.setProductId(product.getId());
		
		productMediaRepository.setInsertUpdateMassive(product.getImages());

		this.setIds(product);
	}	

	private void setIds(Product product)
	{
		List<ProductMedia> listBD = this.getSelectByProduct(product.getId(), new Pagination("id ASC"));
		
		ProductMedia aux = null;
		for(ProductMedia pm:product.getImages())
		{
			aux = this.getOfListById(pm.getOrderIndex(), pm.getUrl(), listBD);
			
			pm.setId(aux.getId());
		}
	}
	
	private ProductMedia getOfListById(int orderIndex,String url,List<ProductMedia> list)
	{	
		Predicate<ProductMedia> byUrl = item -> item.getOrderIndex() == orderIndex && item.getUrl().equals(url);
		
		ProductMedia p = list.stream()
				  .filter(byUrl)
				  .findAny()
				  .orElse(null);
		
		return p;
	}	
}