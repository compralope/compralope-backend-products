package pe.compralo.products.service.implementation;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.compralo.products.repository.interfaces.VariantRepository;
import pe.compralo.products.application.domain.Product;
import pe.compralo.products.service.interfaces.AttributeService;
import pe.compralo.products.service.interfaces.VariantDetailService;
import pe.compralo.products.service.interfaces.VariantService;
import pe.compralo.products.application.domain.Variant;
import pe.compralo.products.application.domain.VariantDetail;
import pe.compralo.products.cross.variables.Parameters;
import pe.compralo.products.application.domain.Pagination;

@Service
public class VariantServiceImp implements VariantService {

	@Autowired
	private VariantRepository variantRepository;
	
	@Autowired
	private AttributeService attributeService;	
	
	@Autowired
	private VariantDetailService variantDetailService;	

	@Override
	public Variant getSelect(int id) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("id",id);

		return variantRepository.getSearch(parameters);
	}

	@Override
	public List<Variant> getSelect(int id, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		if(id > 0) parameters.put("id",id);

		return variantRepository.getSearch(parameters, pagination);
	}
	
	@Override
	public List<Variant> getSelectByProduct(int productId, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("product_id",productId);

		return variantRepository.getSearch(parameters, pagination);
	}	
	
	@Override
	public List<Variant> getSelectByProductResponse(int productId) {
		List<Variant> variants = this.getSelectByProduct(productId, new Pagination("title asc"));
		
		List<VariantDetail> details = variantDetailService.getSelectByProductByActive(productId, new Pagination("id asc"));
		
		for(Variant v:variants)
			this.setDetails(v, details);
			
		return variants;
	}		
	
	private void setDetails(Variant v,List<VariantDetail> details)
	{   
        Predicate<VariantDetail> byVariantParent = item -> item.getVariantId() == v.getId();
		
        details.stream().filter(byVariantParent).forEachOrdered((item) -> {
            v.getDetails().add(item);
        });
	}
	
	private Map<String,Integer> getSelectByProductHashMap(int productId) {
		List<Variant> l = this.getSelectByProduct(productId, new Pagination("code asc"));

		Map<String,Integer> m = new HashMap<String, Integer>();
		for(Variant v:l)
			m.put(v.getCode(), v.getId());
		
		return m;
	}		

	@Override
	public List<Variant> getList(int id, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		if(id > 0) parameters.put("id",id);

		return variantRepository.getFindAll(parameters, pagination);
	}

	@Override
	public void setRegister(Variant o) {
		variantRepository.setInsertUpdate(o);
	}

	@Override
	public void setRegisterMassive(Product product) {
		attributeService.setRegisterMassive(product.getVariants());
		
		this.setInfoVariants(product);
		
		variantRepository.setInsertUpdateMassive(product.getVariants());
		
		this.setVariantId(product);
		
		if(!product.getEnableVariant()) return;
		
		variantDetailService.setRegisterMassive(product);
	}	
	
	private void setInfoVariants(Product product)
	{
		String title = null;
		
		for(Variant v:product.getVariants())
		{
			v.setProductId(product.getId());
			
			if(!product.getEnableVariant())
			{
				v.setCode(product.getCode());
				v.setActive(product.getActive());
			}
			
			v.setStateId(v.getActive() ? Parameters.State_Product_Active : Parameters.State_Product_Inactive);

			title = product.getTitle();
			for(VariantDetail vd:v.getDetails())
				title += " - " + vd.getAttributeTitle() + " " + vd.getAttributeItemsTitle();
			
			v.setTitle(title);
		}		
	}
	
	private void setVariantId(Product product)
	{
		Map<String,Integer> variants = this.getSelectByProductHashMap(product.getId());
		
		for(Variant v:product.getVariants())
		{
			v.setId(variants.get(v.getCode()));
			
			for(VariantDetail vd:v.getDetails()){
				vd.setVariantId(v.getId());
				vd.setActive(true);
			}

		}		
	}	
	
	public void setUpdate(int id,Variant o) {
		o.setId(id);

		variantRepository.setInsertUpdate(o);
	}



}