package pe.compralo.products.service.implementation;

import java.util.List;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import pe.compralo.products.repository.interfaces.ProductRepository;
import pe.compralo.products.service.interfaces.CategoryService;
import pe.compralo.products.service.interfaces.ProductMediaService;
import pe.compralo.products.service.interfaces.ProductService;
import pe.compralo.products.service.interfaces.VariantService;
import pe.compralo.products.application.domain.Product;
import pe.compralo.products.application.domain.ProductMedia;
import pe.compralo.products.application.domain.Variant;
import pe.compralo.products.cross.utils.ConvertFormat;
import pe.compralo.products.cross.variables.CodeHttpRequest;
import pe.compralo.products.cross.variables.MessagesValidation;
import pe.compralo.products.cross.variables.Parameters;
import pe.compralo.products.application.domain.Category;
import pe.compralo.products.application.domain.Messenger;
import pe.compralo.products.application.domain.Pagination;

@Service
public class ProductServiceImp implements ProductService {

	@Autowired
	private Environment yml;		
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private VariantService variantService;
	
	@Autowired
	private ProductMediaService productMediaService;
	
	@Autowired
	private CategoryService categoryService;	
	
	private String urlBase;

    @PostConstruct
	public void initializeParameters() {
        this.urlBase = yml.getProperty("parameters.urlBase");
    }	
	
	
	@Override
	public Product getSelect(int id) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("id",id);

		return productRepository.getSearch(parameters);
	}
	
	@Override
	public Product getSelectByCode(int tenantId, String code) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("tenant_id",tenantId);
		parameters.put("code",code);

		return productRepository.getSearch(parameters);
	}		
	
	@Override
	public Product getSelectView(int id) {
		Product product = this.getSelect(id);
		
		if(product==null) return null;
		
		List<Variant> variants = variantService.getSelectByProductResponse(product.getId());
		
		product.getVariants().addAll(variants);
		
		List<ProductMedia> media = productMediaService.getSelectActiveByProduct(product.getId());
		
		product.getImages().addAll(media);
		
		return product;
	}	

	@Override
	public List<Product> getSelect(int id, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		if(id > 0) parameters.put("id",id);

		return productRepository.getSearch(parameters, pagination);
	}
	


	@Override
	public List<Product> getList(int id, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		if(id > 0) parameters.put("id",id);

		return productRepository.getFindAll(parameters, pagination);
	}

	@Override
	public List<Product> getListByInbox(int tenantId,String title,String listCategoryId,int searchType,Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		if(tenantId > 0) parameters.put("tenant_id",tenantId);
		if(!ConvertFormat.IsNullOrEmpty(listCategoryId)) parameters.put("list_category_id",listCategoryId);
		if(!title.isEmpty()) parameters.put("title",title);
		
		switch(searchType)
		{
			case Parameters.Product_Inbox_Search_Type_Out_Of_Stock:
				parameters.put("out_of_stock",true);
				break;
			case Parameters.Product_Inbox_Search_Type_Enabled:
				parameters.put("active",true);
				break;				
			case Parameters.Product_Inbox_Search_Type_Disabled:
				parameters.put("active",false);
				break;			
			case Parameters.Product_Inbox_Search_Type_Sorting_Title_Asc:
				pagination.setSort("title asc");
				break;								
			case Parameters.Product_Inbox_Search_Type_Sorting_Title_Desc:
				pagination.setSort("title desc");
				break;							
			case Parameters.Product_Inbox_Search_Type_Sorting_Price_Asc:
				pagination.setSort("selling_price_max asc");
				break;								
			case Parameters.Product_Inbox_Search_Type_Sorting_Price_Desc:
				pagination.setSort("selling_price_max desc");
				break;											
		}
		
		return productRepository.getFindAll(parameters, pagination);
	}	
	
	@Override
	public List<Product> getListAutocompleteByTenant(int tenantId,Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("tenant_id",tenantId);
		parameters.put("active",true);
		parameters.put("with_stock",true);
		
		return productRepository.getFindAll(parameters, pagination);
	}	
	
	@Override
	public Messenger setRegister(Product o) {	
		Product exists = this.getSelectByCode(o.getTenantId(), o.getCode());
		
		if(exists!=null) return new Messenger(CodeHttpRequest.Code_Bad_Request,MessagesValidation.Product_SKU_Exists);
		
		//Register this product
		o.setRegisterDate(new Date());
		o.setStateId(Parameters.State_Product_Active);
		o.setActive(true);
	
		this.setRegisterUpdate(o);
		
		//Register your variants
		variantService.setRegisterMassive(o);
		
		productMediaService.setRegisterMassive(o);
		
		return null;
	}

	@Override
	public void setUpdate(int id,Product o) {
		//Register this product
		o.setId(id);
		
		o.setStateId(o.getActive() ? Parameters.State_Product_Active : Parameters.State_Product_Inactive);
		o.setUpdateDate(new Date());	
		
		this.setRegisterUpdate(o);
		
		//Register your variants
		variantService.setRegisterMassive(o);		
		
		productMediaService.setRegisterMassive(o);
	}	
	
	
	private void setRegisterUpdate(Product o)
	{
		Category category = categoryService.setCreate(o.getTenantId(), o.getCategoryTitle());
		
		o.setCategoryId(category.getId());
		
		String url = this.urlBase + "/" + Parameters.Path_Order_Product + "/" + o.getTenantId() + Parameters.Separator_Order_Product + o.getCode() + Parameters.Separator_Order_Product + o.getTitle().replaceAll(" ","_");
		
		o.setUrl(url);
		
		productRepository.setInsertUpdate(o);
	}
	
	@Override
	public Product setUpdateState(int id,Product o) {
		Product p = this.getSelect(id);
		
		p.setActive(o.getActive());
		p.setStateId(p.getActive() ? Parameters.State_Product_Active : Parameters.State_Product_Inactive);
		p.setUpdateDate(new Date());
			
		productRepository.setInsertUpdate(p);
		
		return p;
	}
}