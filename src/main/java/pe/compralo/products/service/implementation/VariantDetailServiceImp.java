package pe.compralo.products.service.implementation;

import java.util.List;
import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.compralo.products.repository.interfaces.VariantDetailRepository;
import pe.compralo.products.service.interfaces.VariantDetailService;
import pe.compralo.products.application.domain.VariantDetail;
import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.application.domain.Product;
import pe.compralo.products.application.domain.Variant;

@Service
public class VariantDetailServiceImp implements VariantDetailService {

	@Autowired
	private VariantDetailRepository variantDetailRepository;

	@Override
	public VariantDetail getSelect(int id) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("id",id);

		return variantDetailRepository.getSearch(parameters);
	}

	@Override
	public List<VariantDetail> getSelect(int id, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		if(id > 0) parameters.put("id",id);

		return variantDetailRepository.getSearch(parameters, pagination);
	}
	
	@Override
	public List<VariantDetail> getSelectByProduct(int productId, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("product_id",productId);
		
		return variantDetailRepository.getSearch(parameters, pagination);
	}	
	
	@Override
	public List<VariantDetail> getSelectByProductByActive(int productId, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("product_id",productId);
		parameters.put("active",true);
		
		return variantDetailRepository.getSearch(parameters, pagination);
	}		

	@Override
	public List<VariantDetail> getList(int id, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		if(id > 0) parameters.put("id",id);

		return variantDetailRepository.getFindAll(parameters, pagination);
	}

	@Override
	public void setRegister(VariantDetail o) {
		variantDetailRepository.setInsertUpdate(o);
	}

	@Override
	public void setRegisterMassive(Product product) {

		List<VariantDetail> list = this.setComplementsIdDetails(product);
		
		variantDetailRepository.setInsertUpdateMassive(list);
		
		this.setIds(product);
	}

	private void setIds(Product product)
	{
		List<VariantDetail> complements = this.getSelectByProduct(product.getId(), new Pagination("variant_id asc"));
		
		VariantDetail aux = null;
		
		for(Variant v:product.getVariants())			
			for(VariantDetail vd:v.getDetails())
			{
				aux = this.getOfListById(vd.getVariantId(), vd.getAttributeItemsId(), complements);
				
				vd.setId(aux.getId());
			}	
	}
	
	private List<VariantDetail> setComplementsIdDetails(Product product)
	{
		//Obtengo todos los detalles
		List<VariantDetail> details = new ArrayList<VariantDetail>();
		
		for(Variant v:product.getVariants())			
			for(VariantDetail vd:v.getDetails())
			{
				vd.setActive(true);
				details.add(vd);
			}
						
		List<VariantDetail> detailsWithId = details.stream().filter(item -> item.getId() > 0).collect(Collectors.toList());
		
		List<VariantDetail> detailsWithoutId = details.stream().filter(item -> item.getId() <= 0).collect(Collectors.toList());
		
		//Obtengo los detalles que se encuentran en BD
		List<VariantDetail> complements = this.getSelectByProduct(product.getId(), new Pagination("variant_id asc"));
		
		//Todos los detalles que tienen ids los almaceno
		List<VariantDetail> list = new ArrayList<VariantDetail>();
		list.addAll(detailsWithId);
		
		VariantDetail aux = null;
		
		//Busco setear el id en los detalles que existen ya en BD
		for(VariantDetail vd:detailsWithoutId)
		{	
			aux = this.getOfListById(vd.getVariantId(), vd.getAttributeItemsId(), complements);
			
			if(aux == null) continue; 
			
			vd.setActive(true);
			vd.setId(aux.getId());
		}
		
		list.addAll(detailsWithoutId);
		
		//Busco obtener los detalles que no se quieren actualizar y no se envian para inactivarlos
		for(VariantDetail vd:complements)
		{
			aux = this.getOfListById(vd.getId(), details);
			
			if(aux != null) continue;			
			
			aux = this.getOfListById(vd.getVariantId(), vd.getAttributeItemsId(), details);
			
			if(aux != null) continue; 
			
			vd.setActive(false);
			list.add(vd);
		}		
		
		return list;
	}

	private VariantDetail getOfListById(int id,List<VariantDetail> list)
	{	
		Predicate<VariantDetail> byAttributeItemId = item -> item.getId() == id;
		
		VariantDetail vd = list.stream()
				  .filter(byAttributeItemId)
				  .findAny()
				  .orElse(null);
		
		return vd;
	}
	
	private VariantDetail getOfListById(int variantId,int attributeItemId,List<VariantDetail> list)
	{	
		Predicate<VariantDetail> byAttributeItemId = item -> item.getVariantId() == variantId && item.getAttributeItemsId() == attributeItemId;
		
		VariantDetail vd = list.stream()
				  .filter(byAttributeItemId)
				  .findAny()
				  .orElse(null);
		
		return vd;
	}
	
	@Override
	public void setUpdate(int id,VariantDetail o) {
		o.setId(id);

		variantDetailRepository.setInsertUpdate(o);
	}

}