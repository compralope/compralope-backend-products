package pe.compralo.products.service.implementation;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.compralo.products.repository.interfaces.AttributeItemsRepository;
import pe.compralo.products.service.interfaces.AttributeItemsService;
import pe.compralo.products.application.domain.AttributeItems;
import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.application.domain.Variant;
import pe.compralo.products.application.domain.VariantDetail;
import pe.compralo.products.cross.utils.ConvertFormat;

@Service
public class AttributeItemsServiceImp implements AttributeItemsService {

	@Autowired
	private AttributeItemsRepository attributeItemsRepository;

	@Override
	public AttributeItems getSelect(int id) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("id",id);

		return attributeItemsRepository.getSearch(parameters);
	}

	@Override
	public List<AttributeItems> getSelect(int id, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		if(id > 0) parameters.put("id",id);

		return attributeItemsRepository.getSearch(parameters, pagination);
	}
	
	@Override
	public List<AttributeItems> getSelectByActive(Boolean active, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("active",active);

		return attributeItemsRepository.getSearch(parameters, pagination);
	}	
	
	@Override
	public Map<String, Integer> getSelectByActiveHashMap() {
		List<AttributeItems> l = this.getSelectByActive(true, new Pagination("title ASC"));
		
		Map<String,Integer> m = new HashMap<String,Integer>();
		
		for(AttributeItems a:l)
			m.put(a.getAttributeId() + " - " + a.getTitle(), a.getId());
		
		return m;
	}			

	@Override
	public List<AttributeItems> getList(int id, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		if(id > 0) parameters.put("id",id);

		return attributeItemsRepository.getFindAll(parameters, pagination);
	}
	
	@Override
	public List<AttributeItems> getListByTenant(int tenantId, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("tenant_id",tenantId);

		return attributeItemsRepository.getFindAll(parameters, pagination);
	}

	@Override
	public void setRegister(AttributeItems o) {
		attributeItemsRepository.setInsertUpdate(o);
	}

	@Override
	public void setUpdate(int id,AttributeItems o) {
		o.setId(id);

		attributeItemsRepository.setInsertUpdate(o);
	}

	@Override
	public void setRegisterMassive(List<VariantDetail> details) {
		List<AttributeItems> list = this.getDistinctAttributeItemsByVariantDetails(details);		
		
		attributeItemsRepository.setInsertUpdateMassive(list);
	}	
	
	@Override
	public List<VariantDetail> getDistinctVariantDetailsByVariants(List<Variant> variants)
	{
		List<VariantDetail> l = new ArrayList<VariantDetail>();
		
		for(Variant v:variants)
			for(VariantDetail vd:v.getDetails())
				l.add(vd);
		
		l = l.stream() 
			  .filter(ConvertFormat.distinctByKey(p -> p.getAttributeTitle() + " - " + p.getAttributeItemsTitle())) 
			  .collect(Collectors.toList());
		
		return l;
	}	
	
	private List<AttributeItems> getDistinctAttributeItemsByVariantDetails(List<VariantDetail> details)
	{
		List<VariantDetail> aux = details.stream() 
			  .filter(ConvertFormat.distinctByKey(p -> p.getAttributeTitle() + " - " + p.getAttributeItemsTitle())) 
			  .collect(Collectors.toList());
		
		List<AttributeItems> l = new ArrayList<AttributeItems>();

		for(VariantDetail vd:aux)
			l.add(new AttributeItems(vd.getAttributeId(),vd.getAttributeItemsTitle(),true));
		
		return l;
	}		
}