package pe.compralo.products.service.implementation;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.compralo.products.repository.interfaces.AttributeRepository;
import pe.compralo.products.service.interfaces.AttributeItemsService;
import pe.compralo.products.service.interfaces.AttributeService;
import pe.compralo.products.application.domain.Attribute;
import pe.compralo.products.application.domain.AttributeItems;
import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.application.domain.Variant;
import pe.compralo.products.application.domain.VariantDetail;
import pe.compralo.products.cross.utils.ConvertFormat;

@Service
public class AttributeServiceImp implements AttributeService {

	@Autowired
	private AttributeRepository attributeRepository;
	
	@Autowired
	private AttributeItemsService attributeItemsService;

	@Override
	public Attribute getSelect(int id) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("id",id);

		return attributeRepository.getSearch(parameters);
	}

	@Override
	public List<Attribute> getSelect(int id, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		if(id > 0) parameters.put("id",id);

		return attributeRepository.getSearch(parameters, pagination);
	}
	
	@Override
	public List<Attribute> getSelectByActive(Boolean active, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("active",active);

		return attributeRepository.getSearch(parameters, pagination);
	}	
	
	private Map<String, Integer> getSelectByActiveHashMap() {
		List<Attribute> l = this.getSelectByActive(true, new Pagination("title ASC"));
		
		Map<String,Integer> m = new HashMap<String,Integer>();
		
		for(Attribute a:l)
			m.put(a.getTitle(), a.getId());
		
		return m;
	}		

	@Override
	public List<Attribute> getList(int id, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		if(id > 0) parameters.put("id",id);

		return attributeRepository.getFindAll(parameters, pagination);
	}
	
	@Override
	public List<Attribute> getListByTenant(int tenantId,Boolean active, Pagination pagination) {
		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("tenantId",tenantId);
		parameters.put("active",active);

		return attributeRepository.getFindAll(parameters, pagination);
	}	
	
	@Override
	public List<Attribute> getListByTenantWithItems(int tenantId,Boolean active, Pagination pagination) {
		List<Attribute> attributes = this.getListByTenant(tenantId, active, pagination);
		
		List<AttributeItems> items = attributeItemsService.getListByTenant(tenantId, pagination);
		
		//set items by attribute
		for(Attribute a:attributes)
			this.setItems(a, items);
		
		return attributes;
	}		
	
	private void setItems(Attribute a,List<AttributeItems> items)
	{   
        Predicate<AttributeItems> byAttributeParent = item -> item.getAttributeId() == a.getId();
		
        items.stream().filter(byAttributeParent).forEachOrdered((item) -> {
            a.getItems().add(item);
        });
	}

	@Override
	public void setRegister(Attribute o) {
		attributeRepository.setInsertUpdate(o);
	}
	
	@Override
	public void setRegisterMassive(List<Variant> variants) {
		//Retiro las variables repetidas
		List<VariantDetail> details = attributeItemsService.getDistinctVariantDetailsByVariants(variants);		
		
		//Obtengo los atributos y los registro
		List<Attribute> list = this.getDistinctAttributeByVariantDetails(details);
		
		attributeRepository.setInsertUpdateMassive(list);

		//Obtengo los ids de los atributos generados y los seteo a los atributos items
		Map<String,Integer> attributes = this.getSelectByActiveHashMap();
		
		this.setVariantDetailsWithAttributeId(details, attributes);
		
		attributeItemsService.setRegisterMassive(details);
		
		//Obtengo los ids de los atributos items generados y los seteo a las variantes details
		Map<String,Integer> attributeItems = attributeItemsService.getSelectByActiveHashMap();
		
		this.setVariantsWithAttributeItemsId(variants, attributes, attributeItems);
	}
	
	
	private List<Attribute> getDistinctAttributeByVariantDetails(List<VariantDetail> details)
	{
		List<VariantDetail> aux = details.stream() 
			  .filter(ConvertFormat.distinctByKey(p -> p.getAttributeTitle())) 
			  .collect(Collectors.toList());
		
		List<Attribute> l = new ArrayList<Attribute>();

		for(VariantDetail vd:aux)
			l.add(new Attribute(vd.getAttributeTitle(),true));
		
		return l;
	}	
	
	private void setVariantDetailsWithAttributeId(List<VariantDetail> details,Map<String,Integer> attributes)
	{
		for(VariantDetail vd:details)
			vd.setAttributeId(attributes.get(vd.getAttributeTitle()));
	}	
	
	private void setVariantsWithAttributeItemsId(List<Variant> variants,Map<String,Integer> attributes,Map<String,Integer> attributeItems)
	{		
		for(Variant v:variants)
			for(VariantDetail vd:v.getDetails())
			{
				vd.setAttributeId(attributes.get(vd.getAttributeTitle()));
				
				vd.setAttributeItemsId(attributeItems.get(vd.getAttributeId() + " - " + vd.getAttributeItemsTitle()));
			}
	}		

	@Override
	public void setUpdate(int id,Attribute o) {
		o.setId(id);

		attributeRepository.setInsertUpdate(o);
	}

}