package pe.compralo.products.service.interfaces;

import java.util.List;

import pe.compralo.products.application.domain.Attribute;
import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.application.domain.Variant;

public interface AttributeService {

	Attribute getSelect(int id);

	List<Attribute> getSelect(int id, Pagination pagination);
	
	List<Attribute> getSelectByActive(Boolean active, Pagination pagination);

	List<Attribute> getList(int id, Pagination pagination);
	
	List<Attribute> getListByTenant(int tenantId,Boolean active, Pagination pagination);
	
	List<Attribute> getListByTenantWithItems(int tenantId,Boolean active, Pagination pagination);

	void setRegister(Attribute o);
	
	void setRegisterMassive(List<Variant> variants);

	void setUpdate(int id,Attribute o);

}