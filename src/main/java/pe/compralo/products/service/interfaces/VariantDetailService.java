package pe.compralo.products.service.interfaces;

import java.util.List;

import pe.compralo.products.application.domain.VariantDetail;
import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.application.domain.Product;

public interface VariantDetailService {

	VariantDetail getSelect(int id);

	List<VariantDetail> getSelect(int id, Pagination pagination);
	
	List<VariantDetail> getSelectByProduct(int productId, Pagination pagination);
	
	List<VariantDetail> getSelectByProductByActive(int productId, Pagination pagination);

	List<VariantDetail> getList(int id, Pagination pagination);

	void setRegister(VariantDetail o);
	
	void setRegisterMassive(Product product);

	void setUpdate(int id,VariantDetail o);
	
	

}