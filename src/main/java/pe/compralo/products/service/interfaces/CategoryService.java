package pe.compralo.products.service.interfaces;

import java.util.List;

import pe.compralo.products.application.domain.Category;
import pe.compralo.products.application.domain.Pagination;

public interface CategoryService {

	Category getSelect(int id);

	List<Category> getSelect(int id, Pagination pagination);
	
	Category getSelectByTenantTitle(int tenantId, String title);

	List<Category> getList(int id, Pagination pagination);
	
	List<Category> getListByTenant(int tenantId,Boolean active, Pagination pagination);

	void setRegister(Category o);

	Category setCreate(int tenantId, String title);
		
	void setUpdate(int id,Category o);

}