package pe.compralo.products.service.interfaces;

import java.util.List;

import pe.compralo.products.application.domain.Variant;
import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.application.domain.Product;

public interface VariantService {

	Variant getSelect(int id);

	List<Variant> getSelect(int id, Pagination pagination);
	
	List<Variant> getSelectByProduct(int productId, Pagination pagination);
	
	List<Variant> getSelectByProductResponse(int productId);

	List<Variant> getList(int id, Pagination pagination);

	void setRegister(Variant o);
	
	void setRegisterMassive(Product product);

	void setUpdate(int id,Variant o);

}