package pe.compralo.products.service.interfaces;

import java.util.List;

import pe.compralo.products.application.domain.ProductMedia;
import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.application.domain.Product;

public interface ProductMediaService {

	ProductMedia getSelect(int id);

	List<ProductMedia> getSelect(int id, Pagination pagination);
	
	List<ProductMedia> getSelectByProduct(int productId, Pagination pagination);
	
	List<ProductMedia> getSelectActiveByProduct(int productId);

	List<ProductMedia> getList(int id, Pagination pagination);

	void setRegister(ProductMedia o);

	void setUpdate(int id,ProductMedia o);

	void setRegisterMassive(Product product);
}