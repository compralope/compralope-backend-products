package pe.compralo.products.service.interfaces;

import java.util.List;
import java.util.Map;

import pe.compralo.products.application.domain.AttributeItems;
import pe.compralo.products.application.domain.Pagination;
import pe.compralo.products.application.domain.Variant;
import pe.compralo.products.application.domain.VariantDetail;

public interface AttributeItemsService {

	AttributeItems getSelect(int id);

	List<AttributeItems> getSelect(int id, Pagination pagination);
	
	List<AttributeItems> getSelectByActive(Boolean active, Pagination pagination);
	
	Map<String, Integer> getSelectByActiveHashMap();

	List<AttributeItems> getList(int id, Pagination pagination);
	
	List<AttributeItems> getListByTenant(int tenantId, Pagination pagination);	

	void setRegister(AttributeItems o);

	void setUpdate(int id,AttributeItems o);
	
	void setRegisterMassive(List<VariantDetail> details);

	List<VariantDetail> getDistinctVariantDetailsByVariants(List<Variant> variants);
}