package pe.compralo.products.service.interfaces;

import java.util.List;

import pe.compralo.products.application.domain.Product;
import pe.compralo.products.application.domain.Messenger;
import pe.compralo.products.application.domain.Pagination;

public interface ProductService {

	Product getSelect(int id);
	
	Product getSelectView(int id);

	List<Product> getSelect(int id, Pagination pagination);
	
	Product getSelectByCode(int tenantId, String code);

	List<Product> getList(int id, Pagination pagination);
	
	List<Product> getListByInbox(int tenantId,String title,String listCategoryId,int searchType,Pagination pagination);
	
	List<Product> getListAutocompleteByTenant(int tenantId,Pagination pagination);

	Messenger setRegister(Product o);

	void setUpdate(int id,Product o);
	
	Product setUpdateState(int id,Product o);

}