
CREATE TABLE dbo.attribute
( 
	id                   int IDENTITY ( 1,1 ) ,
	title                varchar(256)  NULL ,
	active               bit  NULL 
)
go



ALTER TABLE dbo.attribute
	ADD CONSTRAINT PK_attribute_id PRIMARY KEY (id ASC)
go


