-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.attribute_insert_update
(
	@id int OUTPUT,
	@title varchar(256),
	@active bit
)
AS
BEGIN

	IF NOT EXISTS (SELECT 1 FROM dbo.attribute WHERE id= @id)
	BEGIN
		INSERT INTO dbo.attribute
		(
			title,
			active
		)
		VALUES
		(
			@title,
			@active
		);

		SET @id = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE dbo.attribute SET
		title = @title,
		active = @active
		WHERE id = @id;
	END;

END;

GO