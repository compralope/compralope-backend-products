-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.category_insert_update
(
	@id int OUTPUT,
	@tenant_id int,
	@category_parent_id int,
	@title varchar(256),
	@active bit
)
AS
BEGIN

	IF NOT EXISTS (SELECT 1 FROM dbo.category WHERE id= @id)
	BEGIN
		INSERT INTO dbo.category
		(
			tenant_id,
			category_parent_id,
			title,
			active
		)
		VALUES
		(
			@tenant_id,
			@category_parent_id,
			@title,
			@active
		);

		SET @id = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE dbo.category SET
		tenant_id = @tenant_id,
		category_parent_id = @category_parent_id,
		title = @title,
		active = @active
		WHERE id = @id;
	END;

END;

GO