-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.variant_detail_insert_update_massive
(
	@json_data NTEXT 
)
AS

--EXECUTE dbo.variant_detail_insert_update_massive '[{"variantId": 1, "atrributeItemsId": "3", "active": true }]';

BEGIN

	MERGE INTO dbo.variant_detail [TBL]
	USING
	(
		SELECT * FROM OPENJSON(@json_data)
		WITH 
		(
			id INT,
			variantId INT,
			attributeItemsId INT,
			active bit
		)
	) [TMP]
	(
		id,
		variant_id,
		attribute_items_id,
		active
	)
	ON 
	(
		TBL.id = TMP.id
	)
	WHEN NOT MATCHED THEN
		Insert 
		(
			variant_id,
			attribute_items_id,
			active
		)
		Values
		(
			TMP.variant_id,
			TMP.attribute_items_id,
			TMP.active
		)
	WHEN MATCHED THEN
		UPDATE SET
        variant_id = TMP.variant_id,
		attribute_items_id	=	TMP.attribute_items_id,	
		active	=	TMP.active;	

END;

GO