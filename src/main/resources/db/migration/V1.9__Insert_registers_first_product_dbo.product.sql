﻿DECLARE @talla_M INT,@color_azul INT, @talla_S INT,  @talla_L INT, @color_rojo INT, @color_blanco INT, @color_negro int;

INSERT INTO dbo.attribute
(
    title,
    active
)
VALUES
(   'Talla',  -- title - varchar(256)
    1 -- active - bit
    );

INSERT INTO dbo.attribute
(
    title,
    active
)
VALUES
(   'Color',  -- title - varchar(256)
    1 -- active - bit
    );

INSERT INTO	dbo.attribute_items
(
    attribute_id,
    title,
    active
)
VALUES
(   1,   -- attribute_id - int
    'S',  -- title - varchar(256)
    1 -- active - bit
    );

SET @talla_S = SCOPE_IDENTITY();

INSERT INTO	dbo.attribute_items
(
    attribute_id,
    title,
    active
)
VALUES
(   1,   -- attribute_id - int
    'M',  -- title - varchar(256)
    1 -- active - bit
    );

SET @talla_M = SCOPE_IDENTITY();

INSERT INTO	dbo.attribute_items
(
    attribute_id,
    title,
    active
)
VALUES
(   1,   -- attribute_id - int
    'L',  -- title - varchar(256)
    1 -- active - bit
    );

SET @talla_L = SCOPE_IDENTITY();

INSERT INTO	dbo.attribute_items
(
    attribute_id,
    title,
    active
)
VALUES
(   2,   -- attribute_id - int
    'Azul',  -- title - varchar(256)
    1 -- active - bit
    );

SET @color_azul = SCOPE_IDENTITY();

INSERT INTO	dbo.attribute_items
(
    attribute_id,
    title,
    active
)
VALUES
(   2,   -- attribute_id - int
    'Rojo',  -- title - varchar(256)
    1 -- active - bit
    );

SET @color_rojo = SCOPE_IDENTITY();


INSERT INTO	dbo.attribute_items
(
    attribute_id,
    title,
    active
)
VALUES
(   2,   -- attribute_id - int
    'Negro',  -- title - varchar(256)
    1 -- active - bit
    );

SET @color_negro = SCOPE_IDENTITY();

INSERT INTO	dbo.attribute_items
(
    attribute_id,
    title,
    active
)
VALUES
(   2,   -- attribute_id - int
    'Blanco',  -- title - varchar(256)
    1 -- active - bit
    );

SET @color_blanco = SCOPE_IDENTITY();

DECLARE @product_id_1 INT;

INSERT INTO dbo.product
(
    tenant_id,
    state_id,
    category_id,
	currency_id,
    code,
    title,
    details,
    url,
    enable_igv,
    enable_variant,
    active,
    register_date,
    update_date
)
VALUES
(   1,         -- tenant_id - int
    1,         -- state_id - int
    1,         -- category_id - int
	1,
    'PUD00001',        -- code - varchar(32)
    'Pantalon Dunkelvolk',        -- title - varchar(256)
    N'',       -- details - ntext
    'https://compralope.com/products/Pantalon_Dunkelvolk',        -- url - varchar(256)
    1,      -- enable_igv - bit
    1,      -- enable_variant - bit
    1,      -- active - bit
    GETDATE(), -- register_date - datetime
    NULL  -- update_date - datetime
    );

SET @product_id_1 = SCOPE_IDENTITY();

DECLARE @variant_id_1 INT;

INSERT INTO dbo.variant
(
    product_id,
    state_id,
	code,
    title,
    selling_price,
    cost_price,
    cost_average,
    stock,
    active
)
VALUES
(   @product_id_1,    -- product_id - int
    1,    -- state_id - int
	'PUD00001LA',
    'Talla L - Color Azul',   -- title - varchar(256)
    120, -- selling_price - decimal(20, 4)
    80, -- cost_price - decimal(20, 4)
    NULL, -- cost_average - decimal(20, 4)
    10, -- stock - decimal(20, 4)
    1  -- active - bit
    );

SET @variant_id_1 = SCOPE_IDENTITY();

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_1, -- variant_id - int
    @talla_L,  -- attribute_items_id - int
	1
    );

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_1, -- variant_id - int
    @color_azul,  -- attribute_items_id - int
	1
    );
	
DECLARE @variant_id_2 INT;

INSERT INTO dbo.variant
(
    product_id,
    state_id,
	code,
    title,
    selling_price,
    cost_price,
    cost_average,
    stock,
    active
)
VALUES
(   @product_id_1,    -- product_id - int
    1,    -- state_id - int
	'PUD00001SR',
    'Talla S - Color Rojo',   -- title - varchar(256)
    120, -- selling_price - decimal(20, 4)
    80, -- cost_price - decimal(20, 4)
    NULL, -- cost_average - decimal(20, 4)
    10, -- stock - decimal(20, 4)
    1  -- active - bit
    );

SET @variant_id_2 = SCOPE_IDENTITY();

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_2, -- variant_id - int
    @talla_S,  -- attribute_items_id - int
	1
    );

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_2, -- variant_id - int
    @color_rojo,  -- attribute_items_id - int
	1
    );

DECLARE @product_id_2 INT;

INSERT INTO dbo.product
(
    tenant_id,
    state_id,
    category_id,
	currency_id,
    code,
    title,
    details,
    url,
    enable_igv,
    enable_variant,
    active,
    register_date,
    update_date
)
VALUES
(   1,         -- tenant_id - int
    1,         -- state_id - int
    1,         -- category_id - int
	1,
    'POL00001',        -- code - varchar(32)
    'Polo Adibas',        -- title - varchar(256)
    N'A los pitucos como a Richie les encanta estos modelos',       -- details - ntext
    'https://compralope.com/products/Polo_Adibas',        -- url - varchar(256)
    1,      -- enable_igv - bit
    1,      -- enable_variant - bit
    1,      -- active - bit
    GETDATE(), -- register_date - datetime
    NULL  -- update_date - datetime
    );

SET @product_id_2 = SCOPE_IDENTITY();

DECLARE @variant_id_3 INT;

INSERT INTO dbo.variant
(
    product_id,
    state_id,
	code,
    title,
    selling_price,
    cost_price,
    cost_average,
    stock,
    active
)
VALUES
(   @product_id_2,    -- product_id - int
    1,    -- state_id - int
	'POL00001MN',
    'Talla M - Color Negro',   -- title - varchar(256)
    120, -- selling_price - decimal(20, 4)
    80, -- cost_price - decimal(20, 4)
    NULL, -- cost_average - decimal(20, 4)
    10, -- stock - decimal(20, 4)
    1  -- active - bit
    );

SET @variant_id_3 = SCOPE_IDENTITY();

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_3, -- variant_id - int
    @talla_M,
	1
    );

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_3, -- variant_id - int
    @color_negro,
	1
    );

DECLARE @variant_id_4 INT;

INSERT INTO dbo.variant
(
    product_id,
    state_id,
	code,
    title,
    selling_price,
    cost_price,
    cost_average,
    stock,
    active
)
VALUES
(   @product_id_2,    -- product_id - int
    1,    -- state_id - int
	'POL00001SB',
    'Talla S - Color Blanco',   -- title - varchar(256)
    230, -- selling_price - decimal(20, 4)
    190, -- cost_price - decimal(20, 4)
    NULL, -- cost_average - decimal(20, 4)
    50, -- stock - decimal(20, 4)
    1  -- active - bit
    );

SET @variant_id_4 = SCOPE_IDENTITY();

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_4, -- variant_id - int
    @talla_S,
	1
    );

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_4, -- variant_id - int
    @color_blanco,
	1
    );

DECLARE @product_id_3 INT;

INSERT INTO dbo.product
(
    tenant_id,
    state_id,
    category_id,
	currency_id,
    code,
    title,
    details,
    url,
    enable_igv,
    enable_variant,
    active,
    register_date,
    update_date
)
VALUES
(   1,         -- tenant_id - int
    1,         -- state_id - int
    1,         -- category_id - int
	1,
    'PAN00001',        -- code - varchar(32)
    'Pantal�n Garrita',        -- title - varchar(256)
    N'Pantalon para pelados y blancos, si eres un chico de san borja, te gustar�',       -- details - ntext
    'https://compralope.com/products/Pantalon_garrita',        -- url - varchar(256)
    1,      -- enable_igv - bit
    1,      -- enable_variant - bit
    1,      -- active - bit
    GETDATE(), -- register_date - datetime
    NULL  -- update_date - datetime
    );

SET @product_id_3 = SCOPE_IDENTITY();

DECLARE @variant_id_5 INT;

INSERT INTO dbo.variant
(
    product_id,
    state_id,
	code,
    title,
    selling_price,
    cost_price,
    cost_average,
    stock,
    active
)
VALUES
(   @product_id_3,    -- product_id - int
    1,    -- state_id - int
	'PAN00001MB',
    'Talla M - Color Blanco',   -- title - varchar(256)
    180, -- selling_price - decimal(20, 4)
    150, -- cost_price - decimal(20, 4)
    NULL, -- cost_average - decimal(20, 4)
    17, -- stock - decimal(20, 4)
    1  -- active - bit
    );

SET @variant_id_5 = SCOPE_IDENTITY();

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_5, -- variant_id - int
    @talla_M,
	1
    );

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_5, -- variant_id - int
    @color_blanco,
	1
    );

DECLARE @variant_id_6 INT;
			
INSERT INTO dbo.variant
(
    product_id,
    state_id,
	code,
    title,
    selling_price,
    cost_price,
    cost_average,
    stock,
    active
)
VALUES
(   @product_id_3,    -- product_id - int
    1,    -- state_id - int
	'PAN00001SR',
    'Talla S - Color Rojo',   -- title - varchar(256)
    220, -- selling_price - decimal(20, 4)
    180, -- cost_price - decimal(20, 4)
    NULL, -- cost_average - decimal(20, 4)
    50, -- stock - decimal(20, 4)
    1  -- active - bit
    );

SET @variant_id_6 = SCOPE_IDENTITY();

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_6, -- variant_id - int
    @talla_S,
	1
    );

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_6, -- variant_id - int
    @color_rojo,
	1
    );

DECLARE @product_id_4 INT;

INSERT INTO dbo.product
(
    tenant_id,
    state_id,
    category_id,
	currency_id,
    code,
    title,
    details,
    url,
    enable_igv,
    enable_variant,
    active,
    register_date,
    update_date
)
VALUES
(   1,         -- tenant_id - int
    1,         -- state_id - int
    1,         -- category_id - int
	1,
    'CAS00001',        -- code - varchar(32)
    'Casaca FrontEnd',        -- title - varchar(256)
    N'Si te gusta trabajar en html y molestar a tus backend con tu pelada, este polo es para ti',       -- details - ntext
    'https://compralope.com/products/casaca_frontend',        -- url - varchar(256)
    1,      -- enable_igv - bit
    1,      -- enable_variant - bit
    1,      -- active - bit
    GETDATE(), -- register_date - datetime
    NULL  -- update_date - datetime
    );

SET @product_id_4 = SCOPE_IDENTITY();

DECLARE @variant_id_7 INT;

INSERT INTO dbo.variant
(
    product_id,
    state_id,
	code,
    title,
    selling_price,
    cost_price,
    cost_average,
    stock,
    active
)
VALUES
(   @product_id_4,    -- product_id - int
    1,    -- state_id - int
	'CAS00001MR',
    'Talla M - Color Rojo',   -- title - varchar(256)
    160, -- selling_price - decimal(20, 4)
    120, -- cost_price - decimal(20, 4)
    NULL, -- cost_average - decimal(20, 4)
    15, -- stock - decimal(20, 4)
    1  -- active - bit
    );

SET @variant_id_7 = SCOPE_IDENTITY();

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_7, -- variant_id - int
    @talla_M,
	1
    );

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_7, -- variant_id - int
    @color_rojo,
	1
    );

DECLARE @variant_id_8 INT;

INSERT INTO dbo.variant
(
    product_id,
    state_id,
	code,
    title,
    selling_price,
    cost_price,
    cost_average,
    stock,
    active
)
VALUES
(   @product_id_4,    -- product_id - int
    1,    -- state_id - int
	'CAS00001SN',
    'Talla S - Color Negro',   -- title - varchar(256)
    120, -- selling_price - decimal(20, 4)
    80, -- cost_price - decimal(20, 4)
    NULL, -- cost_average - decimal(20, 4)
    10, -- stock - decimal(20, 4)
    1  -- active - bit
    );

SET @variant_id_8 = SCOPE_IDENTITY();

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_8, -- variant_id - int
    @talla_S,
	1
    );

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_8, -- variant_id - int
    @color_negro,
	1
    );

DECLARE @product_id_5 INT;

INSERT INTO dbo.product
(
    tenant_id,
    state_id,
    category_id,
	currency_id,
    code,
    title,
    details,
    url,
    enable_igv,
    enable_variant,
    active,
    register_date,
    update_date
)
VALUES
(   1,         -- tenant_id - int
    1,         -- state_id - int
    1,         -- category_id - int
	1,
    'BLA00001',        -- code - varchar(32)
    'Blazer Gusanito',        -- title - varchar(256)
    N'Si eres gerente de ti y quieres meter a tu gente en la nueva empresa donde estas este blazer sera la sensaci�n',       -- details - ntext
    'https://compralope.com/products/blazer_gusanito',        -- url - varchar(256)
    1,      -- enable_igv - bit
    1,      -- enable_variant - bit
    1,      -- active - bit
    GETDATE(), -- register_date - datetime
    NULL  -- update_date - datetime
    );

SET @product_id_5 = SCOPE_IDENTITY();

DECLARE @variant_id_9 INT;

INSERT INTO dbo.variant
(
    product_id,
    state_id,
	code,
    title,
    selling_price,
    cost_price,
    cost_average,
    stock,
    active
)
VALUES
(   @product_id_5,    -- product_id - int
    1,    -- state_id - int
	'BLA00001MB',
    'Talla M - Color blanco',   -- title - varchar(256)
    160, -- selling_price - decimal(20, 4)
    120, -- cost_price - decimal(20, 4)
    NULL, -- cost_average - decimal(20, 4)
    15, -- stock - decimal(20, 4)
    1  -- active - bit
    );

SET @variant_id_9 = SCOPE_IDENTITY();

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_9, -- variant_id - int
    @talla_M,
	1
    );

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_9, -- variant_id - int
    @color_blanco,
	1
    );

DECLARE @variant_id_10 INT;

INSERT INTO dbo.variant
(
    product_id,
    state_id,
	code,
    title,
    selling_price,
    cost_price,
    cost_average,
    stock,
    active
)
VALUES
(   @product_id_5,    -- product_id - int
    1,    -- state_id - int
	'BLA00001SR',
    'Talla S - Color Rojo',   -- title - varchar(256)
    350, -- selling_price - decimal(20, 4)
    250, -- cost_price - decimal(20, 4)
    NULL, -- cost_average - decimal(20, 4)
    44, -- stock - decimal(20, 4)
    1  -- active - bit
    );

SET @variant_id_10 = SCOPE_IDENTITY();

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_10, -- variant_id - int
    @talla_S,  -- attribute_items_id - int
	1
    );

INSERT INTO dbo.variant_detail
(
    variant_id,
    attribute_items_id,
	active
)
VALUES
(   @variant_id_10, -- variant_id - int
    @color_rojo,  -- attribute_items_id - int
	1
    );