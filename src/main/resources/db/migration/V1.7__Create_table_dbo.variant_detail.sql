
CREATE TABLE dbo.variant_detail
( 
	id                   int IDENTITY ( 1,1 ) ,
	variant_id           int  NULL ,
	attribute_items_id   int  NULL ,
	active 				 bit NULL
)
go



ALTER TABLE dbo.variant_detail
	ADD CONSTRAINT PK_variant_detail_id PRIMARY KEY (id ASC)
go




ALTER TABLE dbo.variant_detail
	ADD CONSTRAINT FK_variant_detail_variant_id FOREIGN KEY (variant_id) REFERENCES dbo.variant(id)
go




ALTER TABLE dbo.variant_detail
	ADD CONSTRAINT FK_variant_detail_attribute_items_id FOREIGN KEY (attribute_items_id) REFERENCES dbo.attribute_items(id)
go