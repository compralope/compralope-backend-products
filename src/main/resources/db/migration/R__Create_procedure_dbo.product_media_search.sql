-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.product_media_search
(
	@parameters_json ntext,
	@order_by varchar(128)
)
AS

--EXECUTE dbo.product_media_search '{"product_id":6,"enable":true}','';

DECLARE @sql nvarchar(MAX),@sql_body varchar(5500),@sql_join varchar(1000),@sql_where varchar(1500),@sql_parameter nvarchar(1024);

DECLARE @id int,@product_id int,@enable bit;

--FILTERS
BEGIN
	SET @id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='id'
	);
	
	SET @product_id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='product_id'
	);	

	SET @enable =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='enable'
	);	
	
	
END;

--CONSTRUCT SQL
BEGIN
	--SQL BODY
	BEGIN
		SET @sql_body = '
		SELECT
		PRO.id,
		PRO.product_id,
		PRO.file_storage_id,
		PRO.image_type,
		PRO.[url],
		PRO.order_index,
		PRO.principal,
		PRO.enable';

		SET @sql_join = '
		FROM dbo.product_media [PRO]';
	END;

	--SQL WHERE
	BEGIN
		SET @sql_where = '
		WHERE (1=1)';

		--id
		IF @id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND PRO.id = @id';
		END;
		
		--product_id
		IF @product_id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND PRO.product_id = @product_id';
		END;

		--enable
		IF @enable IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND PRO.enable = @enable';
		END;		
	END;

	--ORDER BY
	BEGIN
		IF @order_by = ''
		BEGIN
			Set @order_by = 'id ASC';
		END;

		SET @order_by = '
		Order By ' + @order_by;
	END;
END;

--EXECUTE SQL
BEGIN
	SET @sql_parameter = '@id int,@product_id int,@enable bit';

	SET @sql = @sql_body + @sql_join + @sql_where + @order_by;

	--PRINT @sql;

	EXECUTE SYS.SP_EXECUTESQL @sql,@sql_parameter,@id,@product_id,@enable;
END;

GO