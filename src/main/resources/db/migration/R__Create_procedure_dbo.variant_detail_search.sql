-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.variant_detail_search
(
	@parameters_json ntext,
	@order_by varchar(128)
)
AS

--EXECUTE dbo.variant_detail_search '{"product_id":4, "active":1}','';

DECLARE @sql nvarchar(MAX),@sql_body varchar(5500),@sql_join varchar(1000),@sql_where varchar(1500),@sql_parameter nvarchar(1024);

DECLARE @id INT,@variant_id int,@product_id int,@active bit;

--FILTERS
BEGIN
	SET @id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='id'
	);

	SET @variant_id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='variant_id'
	);

	SET @product_id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='product_id'
	);
	
	SET @active =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='active'
	);	
END;

--CONSTRUCT SQL
BEGIN
	--SQL BODY
	BEGIN
		SET @sql_body = '
		SELECT
		VAR.id,
		VAR.variant_id,
		ATT.id [atrribute_id],
		ATT.title [atrribute_title],
		VAR.attribute_items_id,
		ATTITM.title [atrribute_items_title],
		VAR.active';

		SET @sql_join = '
		FROM dbo.variant_detail [VAR]
		INNER JOIN dbo.variant [VARCAB] ON VARCAB.id = VAR.variant_id
		INNER JOIN	dbo.attribute_items [ATTITM] ON ATTITM.id = VAR.attribute_items_id
		INNER JOIN dbo.attribute [ATT] ON ATT.id = ATTITM.attribute_id';
	END;

	--SQL WHERE
	BEGIN
		SET @sql_where = '
		WHERE (1=1)';

		--id
		IF @id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND VAR.id = @id';
		END;

		--variant_id
		IF @variant_id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND VAR.variant_id = @variant_id';
		END;

		--product_id
		IF @product_id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND VARCAB.product_id = @product_id';
		END;
		
		--active
		IF @active IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND VAR.active = @active';
		END;		

	END;

	--ORDER BY
	BEGIN
		IF @order_by = ''
		BEGIN
			Set @order_by = 'id ASC';
		END;

		SET @order_by = '
		Order By ' + @order_by;
	END;
END;

--EXECUTE SQL
BEGIN
	SET @sql_parameter = '@id int,@variant_id int,@product_id int,@active bit';

	SET @sql = @sql_body + @sql_join + @sql_where + @order_by;

	--PRINT @sql;

	EXECUTE SYS.SP_EXECUTESQL @sql,@sql_parameter,@id,@variant_id,@product_id,@active;
END;

GO