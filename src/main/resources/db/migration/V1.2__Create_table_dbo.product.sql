CREATE TABLE dbo.product
( 
	id                   int IDENTITY ( 1,1 ) ,
	tenant_id            int  NULL ,
	state_id             int  NULL ,
	category_id          int  NULL ,
	currency_id          int  NULL ,
	code                 varchar(32)  NULL ,
	title                varchar(256)  NULL ,
	details              ntext  NULL ,
	url                  varchar(256)  NULL ,
	enable_igv           bit  NULL ,
	enable_variant       bit  NULL ,
	active               bit  NULL ,
	register_date        datetime  NULL ,
	update_date          datetime  NULL 
)
go



ALTER TABLE dbo.product
	ADD CONSTRAINT PK_product_id PRIMARY KEY (id ASC)
go



CREATE INDEX FK_product_tenant_id ON dbo.product
( 
	tenant_id             ASC
)
go



CREATE INDEX FK_product_state_id ON dbo.product
( 
	state_id              ASC
)
go



CREATE INDEX FK_product_currency_id ON dbo.product
( 
	currency_id           ASC
)
go




ALTER TABLE dbo.product
	ADD CONSTRAINT FK_product_category_id FOREIGN KEY (category_id) REFERENCES dbo.category(id)
go
