-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.attribute_items_find_all
(
	@parameters_json ntext,
	@page_active int,
	@show_quantity int,
	@order_by varchar(128),
	@total_registers int OUTPUT
)
AS

--EXECUTE dbo.attribute_items_find_all '{"tenant_id":1,"id":1}',1,10,'',NULL;

DECLARE @sql nvarchar(MAX),@sql_count nvarchar(MAX),@sql_body varchar(5500),@sql_join varchar(1000),@sql_where varchar(1500),@sql_parameter nvarchar(1024),@sql_parameter_count nvarchar(1024);

DECLARE @rownum_from varchar(10),@rownum_to varchar(10);

DECLARE @id int, @tenant_id int;

--VARIABLES
BEGIN
	SET @rownum_from = Cast((((@page_active - 1) * @show_quantity) + 1) AS varchar);

	SET @rownum_to = Cast((@page_active * @show_quantity) AS varchar);
END;

--FILTERS
BEGIN
	SET @id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='id'
	);

	SET @tenant_id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='tenant_id'
	);

END;

--CONSTRUCT SQL
BEGIN
	--SQL BODY
	BEGIN
		SET @sql_body = '
		SELECT
		ATT.id,
		ATT.attribute_id,
		ATT.title,
		ATT.active';

		SET @sql_join = '
		FROM dbo.attribute_items [ATT]';
	END;

	--SQL WHERE
	BEGIN
		SET @sql_where = '
		WHERE (1=1)';

		--id
		IF @id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND ATT.id = @id';
		END;

		--tenant_id
		IF @tenant_id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND ATT.attribute_id 
			IN
			(
				SELECT AI.attribute_id FROM dbo.variant_detail [VD]	
				INNER JOIN dbo.attribute_items [AI] ON AI.id = VD.attribute_items_id AND VD.active = 1
				INNER JOIN dbo.variant [V] ON V.id = VD.variant_id
				INNER JOIN dbo.product [P] ON P.id = V.product_id
				WHERE P.tenant_id = @tenant_id
			)
			';
		END;

	END;

	--ORDER BY
	BEGIN
		IF @order_by = ''
		BEGIN
			Set @order_by = 'id ASC';
		END;

		SET @order_by = '
		Order By ' + @order_by;
	END;
END;

--EXECUTE SQL
BEGIN
	SET @total_registers = 0;

	SET @sql_parameter = '@id int,@tenant_id int';

	SET @sql_parameter_count = @sql_parameter + ',@total_registers int output';

	SET @sql = @sql_body + @sql_join + @sql_where;

	IF @page_active > 0
	BEGIN
		SET @sql_count = N'SELECT @total_registers = COUNT(1) ' + @sql_join + @sql_where;

		EXECUTE SYS.SP_EXECUTESQL @sql_count,@sql_parameter_count,@id,@tenant_id,@total_registers=@total_registers OUTPUT;

		SET @sql = 'SELECT * FROM (SELECT ROW_NUMBER() OVER (' + @order_by + ') [ROWNUM],* FROM (' + @sql + ') [TMP] ) [PAG] WHERE ROWNUM >= ' + @rownum_from + ' And ROWNUM <= ' + @rownum_to; 
	END
	ELSE
	BEGIN
		SET @sql += @order_by;
	END;

	--PRINT @sql;

	EXECUTE SYS.SP_EXECUTESQL @sql,@sql_parameter,@id,@tenant_id;
END;

GO