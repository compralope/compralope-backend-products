-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.attribute_insert_update_massive
(
	@json_data NTEXT 
)
AS

--EXECUTE dbo.attribute_insert_update_massive '[{"title": "Corte", "active": true }]';

BEGIN

	MERGE INTO dbo.attribute [TBL]
	USING
	(
		SELECT * FROM OPENJSON(@json_data)
		WITH 
		(
			id INT,
			title varchar(256),
			active bit
		)
	) [TMP]
	(
		id,
		title,
		active
	)
	ON 
	(
		TBL.title=TMP.title
	)
	WHEN NOT MATCHED THEN
		Insert 
		(
			title,
			active
		)
		Values
		(
			TMP.title,
			TMP.active
		)
	WHEN MATCHED THEN
		UPDATE Set
		title	=	TMP.title,	
		active	=	TMP.active;	

END;

GO