
CREATE TABLE dbo.attribute_items
( 
	id                   int IDENTITY ( 1,1 ) ,
	attribute_id         int  NULL ,
	title                varchar(256)  NULL ,
	active               bit  NULL 
)
go



ALTER TABLE dbo.attribute_items
	ADD CONSTRAINT PK_attribute_items_id PRIMARY KEY (id ASC)
go




ALTER TABLE dbo.attribute_items
	ADD CONSTRAINT FK_attribute_items_attribute_id FOREIGN KEY (attribute_id) REFERENCES dbo.attribute(id)
go

