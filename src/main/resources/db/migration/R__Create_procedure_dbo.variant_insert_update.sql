-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.variant_insert_update
(
	@id int OUTPUT,
	@product_id int,
	@state_id int,
	@code varchar(32),
	@title varchar(256),
	@selling_price decimal(20,4),
	@cost_price decimal(20,4),
	@cost_average decimal(20,4),
	@stock decimal(20,4),
	@active bit
)
AS
BEGIN

	IF NOT EXISTS (SELECT 1 FROM dbo.variant WHERE id= @id)
	BEGIN
		INSERT INTO dbo.variant
		(
			product_id,
			state_id,
			code,
			title,
			selling_price,
			cost_price,
			cost_average,
			stock,
			active
		)
		VALUES
		(
			@product_id,
			@state_id,
			@code,
			@title,
			@selling_price,
			@cost_price,
			@cost_average,
			@stock,
			@active
		);

		SET @id = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE dbo.variant SET
		product_id = @product_id,
		state_id = @state_id,
		title = @title,
		selling_price = @selling_price,
		cost_price = @cost_price,
		cost_average = @cost_average,
		stock = @stock,
		active = @active
		WHERE id = @id;
	END;

END;

GO