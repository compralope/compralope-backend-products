-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.attribute_items_insert_update_massive
(
	@json_data NTEXT 
)
AS

--EXECUTE dbo.attribute_items_insert_update_massive '[{"attributeId": 1, "title": "XL", "active": true }]';

BEGIN

	MERGE INTO dbo.attribute_items [TBL]
	USING
	(
		SELECT * FROM OPENJSON(@json_data)
		WITH 
		(
			id INT,
			attributeId INT,
			title varchar(256),
			active bit
		)
	) [TMP]
	(
		id,
		attribute_id,
		title,
		active
	)
	ON 
	(
		TBL.attribute_id = TMP.attribute_id
		AND
		TBL.title=TMP.title
	)
	WHEN NOT MATCHED THEN
		Insert 
		(
			attribute_id,
			title,
			active
		)
		Values
		(
			TMP.attribute_id,
			TMP.title,
			TMP.active
		)
	WHEN MATCHED THEN
		UPDATE SET
        attribute_id = TMP.attribute_id,
		title	=	TMP.title,	
		active	=	TMP.active;	

END;

GO