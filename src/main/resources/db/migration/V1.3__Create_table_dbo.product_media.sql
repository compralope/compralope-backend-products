
CREATE TABLE dbo.product_media
( 
	id                   int IDENTITY ( 1,1 ) ,
	product_id           int  NULL ,
	file_storage_id      int  NULL ,
	image_type           int  NULL ,
	url           		 varchar(max)  NULL ,
	order_index          int  NULL ,
	principal            bit  NULL ,
	enable               bit  NULL 
)
go



ALTER TABLE dbo.product_media
	ADD CONSTRAINT PK_product_media_id PRIMARY KEY (id ASC)
go



CREATE INDEX FK_product_media_file_storage_id ON dbo.product_media
( 
	file_storage_id              ASC
)
go




ALTER TABLE dbo.product_media
	ADD CONSTRAINT FK_media_product_id FOREIGN KEY (product_id) REFERENCES dbo.product(id)
go
