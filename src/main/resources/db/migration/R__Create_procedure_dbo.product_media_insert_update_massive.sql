-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.product_media_insert_update_massive
(
	@json_data NTEXT 
)
AS

--EXECUTE dbo.product_media_insert_update_massive '[{"productId":1, "fileStorageId":1, "imageType": 1, "url": "tenant/1", "orderIndex": 1, "principal": true, "enable":true}]';

BEGIN

	MERGE INTO dbo.product_media [TBL]
	USING
	(
		SELECT * FROM OPENJSON(@json_data)
		WITH 
		(
			id INT,
			productId INT,
			fileStorageId INT,
			imageType VARCHAR(32),
			[url] varchar(max),
			orderIndex int,
			principal bit,
			enable bit
		)
	) [TMP]
	(
		id,
	    product_id,
		file_storage_id,
		image_type,
		[url],
		order_index,
		principal,
		enable
	)
	ON 
	(
		TBL.id=TMP.id
	)
	WHEN NOT MATCHED THEN
		Insert 
		(
			product_id,
			file_storage_id,
			image_type,
			[url],
			order_index,
			principal,
			enable
		)
		Values
		(
			TMP.product_id,
			TMP.file_storage_id,
			TMP.image_type,
			TMP.[url],
			TMP.order_index,
			TMP.principal,
			TMP.enable
		)
	WHEN MATCHED THEN
		UPDATE Set
		product_id	=	TMP.product_id,	
		file_storage_id	=	TMP.file_storage_id,		
		[url]	=	TMP.[url],	
		order_index	=	TMP.order_index,	
		principal	=	TMP.principal,	
		enable	=	TMP.enable;

END;

GO