-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.product_search
(
	@parameters_json ntext,
	@order_by varchar(128)
)
AS

--EXECUTE dbo.product_search '{"tenant_id":1,"code": "CAS00001"}','';

DECLARE @sql nvarchar(MAX),@sql_body varchar(5500),@sql_join varchar(1000),@sql_where varchar(1500),@sql_parameter nvarchar(1024);

DECLARE @id int,@tenant_id int,@code varchar(32);

--FILTERS
BEGIN
	SET @id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='id'
	);
	
	SET @tenant_id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='tenant_id'
	);	
	
	SET @code =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='code'
	);		
END;

--CONSTRUCT SQL
BEGIN
	--SQL BODY
	BEGIN
		SET @sql_body = '
		SELECT
		PRO.id,
		PRO.tenant_id,
		PRO.state_id,
		PRO.currency_id,
		STA.title [state_title],
		PRO.category_id,
		CAT.title [category_title],
		PRO.code,
		PRO.title,
		PRO.details,
		PRO.url,
		PRO.enable_igv,
		PRO.enable_variant,
		PRO.active,
		PRO.register_date,
		PRO.update_date';

		SET @sql_join = '
		FROM dbo.product [PRO]
		INNER JOIN dbo.category [CAT] ON CAT.id = PRO.category_id
		INNER JOIN [compralope.parameters].dbo.state [STA] ON STA.id=PRO.state_id';
	END;

	--SQL WHERE
	BEGIN
		SET @sql_where = '
		WHERE (1=1)';

		--id
		IF @id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND PRO.id = @id';
		END;
		
		--tenant_id
		IF @tenant_id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND PRO.tenant_id = @tenant_id';
		END;

		--code
		IF @code IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND PRO.code = @code';
		END;			
	END;

	--ORDER BY
	BEGIN
		IF @order_by = ''
		BEGIN
			Set @order_by = 'id ASC';
		END;

		SET @order_by = '
		Order By ' + @order_by;
	END;
END;

--EXECUTE SQL
BEGIN
	SET @sql_parameter = '@id int,@tenant_id int,@code varchar(32)';

	SET @sql = @sql_body + @sql_join + @sql_where + @order_by;

	--PRINT @sql;

	EXECUTE SYS.SP_EXECUTESQL @sql,@sql_parameter,@id,@tenant_id,@code;
END;

GO