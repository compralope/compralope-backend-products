-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.product_insert_update
(
	@id int OUTPUT,
	@tenant_id int,
	@state_id int,
	@category_id int,
	@currency_id int,
	@code varchar(32),
	@title varchar(256),
	@details ntext,
	@url varchar(256),
	@enable_igv bit,
	@enable_variant bit,
	@active bit,
	@register_date datetime,
	@update_date datetime
)
AS
BEGIN

	IF NOT EXISTS (SELECT 1 FROM dbo.product WHERE id= @id)
	BEGIN
		INSERT INTO dbo.product
		(
			tenant_id,
			state_id,
			category_id,
			currency_id,
			code,
			title,
			details,
			url,
			enable_igv,
			enable_variant,
			active,
			register_date
		)
		VALUES
		(
			@tenant_id,
			@state_id,
			@category_id,
			@currency_id,
			@code,
			@title,
			@details,
			@url,
			@enable_igv,
			@enable_variant,
			@active,
			@register_date
		);

		SET @id = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE dbo.product SET
		state_id = @state_id,
		category_id = @category_id,
		code = @code,
		title = @title,
		details = @details,
		url = @url,
		enable_igv = @enable_igv,
		enable_variant = @enable_variant,
		active = @active,
		update_date = @update_date
		WHERE id = @id;
	END;

END;

GO