-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.category_search
(
	@parameters_json ntext,
	@order_by varchar(128)
)
AS

--EXECUTE dbo.category_search '{"tenant_id":1,"title": "Ropa"}','';

DECLARE @sql nvarchar(MAX),@sql_body varchar(5500),@sql_join varchar(1000),@sql_where varchar(1500),@sql_parameter nvarchar(1024);

DECLARE @id INT,@tenant_id INT,@title VARCHAR(256);

--FILTERS
BEGIN
	SET @id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='id'
	);

	SET @tenant_id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='tenant_id'
	);

	SET @title =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='title'
	);
END;

--CONSTRUCT SQL
BEGIN
	--SQL BODY
	BEGIN
		SET @sql_body = '
		SELECT
		CAT.id,
		CAT.tenant_id,
		CAT.category_parent_id,
		CAT.title,
		CAT.active';

		SET @sql_join = '
		FROM dbo.category [CAT]';
	END;

	--SQL WHERE
	BEGIN
		SET @sql_where = '
		WHERE (1=1)';

		--id
		IF @id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND CAT.id = @id';
		END;

		--tenant_id
		IF @tenant_id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND CAT.tenant_id = @tenant_id';
		END;

		--title
		IF @title IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND CAT.title = @title';
		END;
	END;

	--ORDER BY
	BEGIN
		IF @order_by = ''
		BEGIN
			Set @order_by = 'id ASC';
		END;

		SET @order_by = '
		Order By ' + @order_by;
	END;
END;

--EXECUTE SQL
BEGIN
	SET @sql_parameter = '@id INT,@tenant_id INT,@title VARCHAR(256)';

	SET @sql = @sql_body + @sql_join + @sql_where + @order_by;

	--PRINT @sql;

	EXECUTE SYS.SP_EXECUTESQL @sql,@sql_parameter,@id,@tenant_id,@title;
END;

GO