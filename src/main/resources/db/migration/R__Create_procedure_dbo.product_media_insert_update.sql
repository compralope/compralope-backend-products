-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.product_media_insert_update
(
	@id int OUTPUT,
	@product_id int,
	@file_storage_id int,
	@image_type int,
	@url varchar(max),
	@order_index int,
	@principal bit,
	@enable bit
)
AS
BEGIN

	IF NOT EXISTS (SELECT 1 FROM dbo.product_media WHERE id= @id)
	BEGIN
		INSERT INTO dbo.product_media
		(
			product_id,
			file_storage_id,
			image_type,
			[url],
			order_index,
			principal,
			enable
		)
		VALUES
		(
			@product_id,
			@file_storage_id,
			@image_type,
			@url,
			@order_index,
			@principal,
			@enable

		);

		SET @id = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE dbo.product_media SET
		product_id = @product_id,
		file_storage_id = @file_storage_id,
		image_type = @image_type,
		[url] = @url,
		order_index = @order_index,
		principal = @principal,
		enable = @enable
		WHERE id = @id;
	END;

END;

GO