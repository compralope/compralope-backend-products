
CREATE TABLE dbo.category
( 
	id                   int IDENTITY ( 1,1 ) ,
	tenant_id            int  NULL ,
	category_parent_id   int  NULL ,
	title                varchar(256)  NULL ,
	active               bit  NULL 
)
go



ALTER TABLE dbo.category
	ADD CONSTRAINT PK_category_id PRIMARY KEY (id ASC)
go



CREATE INDEX FK_category_tenant_id ON dbo.category
( 
	tenant_id             ASC
)
go




ALTER TABLE dbo.category
	ADD CONSTRAINT FK_category_parent_id FOREIGN KEY (category_parent_id) REFERENCES dbo.category(id)
go