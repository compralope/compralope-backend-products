-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.attribute_items_insert_update
(
	@id int OUTPUT,
	@attribute_id int,
	@title varchar(256),
	@active bit
)
AS
BEGIN

	IF NOT EXISTS (SELECT 1 FROM dbo.attribute_items WHERE id= @id)
	BEGIN
		INSERT INTO dbo.attribute_items
		(
			attribute_id,
			title,
			active
		)
		VALUES
		(
			@attribute_id,
			@title,
			@active
		);

		SET @id = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE dbo.attribute_items SET
		attribute_id = @attribute_id,
		title = @title,
		active = @active
		WHERE id = @id;
	END;

END;

GO