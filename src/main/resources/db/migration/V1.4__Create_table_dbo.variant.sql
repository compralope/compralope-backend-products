
CREATE TABLE dbo.variant
( 
	id                   int IDENTITY ( 1,1 ) ,
	product_id           int  NOT NULL ,
	state_id             int  NULL ,
	code                 varchar(32)  NULL ,
	title                varchar(256)  NULL ,
	selling_price        decimal(20,4)  NULL ,
	cost_price           decimal(20,4)  NULL ,
	cost_average         decimal(20,4)  NULL ,
	stock                decimal(20,4)  NULL ,
	active               bit  NULL 
)
go



ALTER TABLE dbo.variant
	ADD CONSTRAINT PK_variant_id PRIMARY KEY (id ASC)
go



CREATE INDEX FK_variant_state_id ON dbo.variant
( 
	state_id              ASC
)
go




ALTER TABLE dbo.variant
	ADD CONSTRAINT FK_variant_product_id FOREIGN KEY (product_id) REFERENCES dbo.product(id)
go

