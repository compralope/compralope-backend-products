-- ${flyway:timestamp}
CREATE OR ALTER FUNCTION [dbo].[fn_split]    
(    
	@List nvarchar(max),    
	@SplitOn varchar(5)    
)      
RETURNS @RtnValue table     
(    
	Id int identity(1,1),    
	Value nvarchar(100)    
)     
AS      
BEGIN
	DECLARE @value nvarchar(2000);
	
	SET @List = ISNULL(@List,'');
	
	If @List <> '' 
	Begin
		While (Charindex(@SplitOn,@List)>0)    
		Begin    
			Set @value = (Select ltrim(rtrim(Substring(@List,1,Charindex(@SplitOn,@List)-1))))
			
			If @value<>''
			Begin
				Insert Into @RtnValue (value) 
				Select @value;
			End;

			Set @List = Substring(@List,Charindex(@SplitOn,@List)+len(@SplitOn),len(@List))    
		End    

		Set @value = ltrim(rtrim(@List));

		If @value<>''
		Begin
			Insert Into @RtnValue (value) 
			Select @value;
		End;
	End;
		
	Return;    
END