INSERT INTO dbo.category
(
    tenant_id,
    category_parent_id,
    title,
    active
)
VALUES
(   1,   -- tenant_id - int
    NULL,   -- category_parent_id - int
    'Ropa',  -- title - varchar(256)
    1 -- active - bit
    );

INSERT INTO dbo.category
(
    tenant_id,
    category_parent_id,
    title,
    active
)
VALUES
(   1,   -- tenant_id - int
    NULL,   -- category_parent_id - int
    'Tecnologia',  -- title - varchar(256)
    1 -- active - bit
    );

INSERT INTO dbo.category
(
    tenant_id,
    category_parent_id,
    title,
    active
)
VALUES
(   1,   -- tenant_id - int
    NULL,   -- category_parent_id - int
    'Deco Hogar',  -- title - varchar(256)
    1 -- active - bit
    );