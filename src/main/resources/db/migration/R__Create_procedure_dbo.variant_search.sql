-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.variant_search
(
	@parameters_json ntext,
	@order_by varchar(128)
)
AS

--EXECUTE dbo.variant_search '{"product_id":1}','';

DECLARE @sql nvarchar(MAX),@sql_body varchar(5500),@sql_join varchar(1000),@sql_where varchar(1500),@sql_parameter nvarchar(1024);

DECLARE @id INT,@product_id int;

--FILTERS
BEGIN
	SET @id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='id'
	);

	SET @product_id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='product_id'
	);
END;

--CONSTRUCT SQL
BEGIN
	--SQL BODY
	BEGIN
		SET @sql_body = '
		SELECT
		VAR.id,
		VAR.product_id,
		VAR.state_id,
		VAR.code,
		VAR.title,
		VAR.selling_price,
		VAR.cost_price,
		VAR.cost_average,
		VAR.stock,
		VAR.active';

		SET @sql_join = '
		FROM dbo.variant [VAR]';
	END;

	--SQL WHERE
	BEGIN
		SET @sql_where = '
		WHERE (1=1)';

		--id
		IF @id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND VAR.id = @id';
		END;

		--product_id
		IF @product_id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND VAR.product_id = @product_id';
		END;
	END;

	--ORDER BY
	BEGIN
		IF @order_by = ''
		BEGIN
			Set @order_by = 'id ASC';
		END;

		SET @order_by = '
		Order By ' + @order_by;
	END;
END;

--EXECUTE SQL
BEGIN
	SET @sql_parameter = '@id int,@product_id int';

	SET @sql = @sql_body + @sql_join + @sql_where + @order_by;

	--PRINT @sql;

	EXECUTE SYS.SP_EXECUTESQL @sql,@sql_parameter,@id,@product_id;
END;

GO