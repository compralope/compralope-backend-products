-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.variant_insert_update_massive
(
	@json_data NTEXT 
)
AS

--EXECUTE dbo.variant_insert_update_massive '[{"productId":1, "stateId":1, "code": "PUD00001LA2", "title": "Talla L - Color Azulino", "sellingPrice": 200.0, "costPrice": 180.0, "costAverage":0.0, "stock": 20.0, "active": 1 }]';

BEGIN

	MERGE INTO dbo.variant [TBL]
	USING
	(
		SELECT * FROM OPENJSON(@json_data)
		WITH 
		(
			id INT,
			productId INT,
			stateId INT,
			code VARCHAR(32),
			title varchar(256),
			sellingPrice decimal(20, 4),
			costPrice decimal(20, 4),
			costAverage decimal(20, 4),
			stock decimal(20, 4),
			active bit
		)
	) [TMP]
	(
		id,
	    product_id,
		state_id,
		code,
		title,
		selling_price,
		cost_price,
		cost_average,
		stock,
		active
	)
	ON 
	(
		TBL.product_id=TMP.product_id
		AND TBL.code=TMP.code 
	)
	WHEN NOT MATCHED THEN
		Insert 
		(
			product_id,
			state_id,
			code,
			title,
			selling_price,
			cost_price,
			cost_average,
			stock,
			active
		)
		Values
		(
			TMP.product_id,
			TMP.state_id,
			TMP.code,
			TMP.title,
			TMP.selling_price,
			TMP.cost_price,
			TMP.cost_average,
			TMP.stock,
			TMP.active
		)
	WHEN MATCHED THEN
		UPDATE Set
		product_id	=	TMP.product_id,	
		state_id	=	TMP.state_id,		
		title	=	TMP.title,	
		selling_price	=	TMP.selling_price,	
		cost_price	=	TMP.cost_price,	
		cost_average	=	TMP.cost_average,	
		stock	=	TMP.stock,	
		active	=	TMP.active;	

END;

GO