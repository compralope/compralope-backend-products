-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.product_find_all
(
	@parameters_json ntext,
	@page_active int,
	@show_quantity int,
	@order_by varchar(128),
	@total_registers int OUTPUT
)
AS

--EXECUTE dbo.product_find_all '{"tenant_id":1, "category_id":1,"state_id":1,"title": "panta"}',1,10,'',NULL;

--EXECUTE dbo.product_find_all '{"tenant_id":1, "list_category_id":"1","out_of_stock" : 1}',1,2,'',NULL;

--EXECUTE dbo.product_find_all '{"tenant_id":1, "title" : "Pant", "active": true,"with_stock" : 1}',1,2,'',NULL;

DECLARE @sql nvarchar(MAX),@sql_count nvarchar(MAX),@sql_body varchar(5500),@sql_join varchar(1000),@sql_where varchar(1500),@sql_parameter nvarchar(1024),@sql_parameter_count nvarchar(1024);

DECLARE @rownum_from varchar(10),@rownum_to varchar(10);

DECLARE @tenant_id INT,@title VARCHAR(256),@category_id INT,@list_category_id VARCHAR(256),@state_id INT,@out_of_stock BIT,@with_stock BIT,@active BIT;

--VARIABLES
BEGIN
	SET @rownum_from = Cast((((@page_active - 1) * @show_quantity) + 1) AS varchar);

	SET @rownum_to = Cast((@page_active * @show_quantity) AS varchar);
END;

--FILTERS
BEGIN

	SET @tenant_id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='tenant_id'
	);

	SET @category_id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='category_id'
	);

	SET @list_category_id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='list_category_id'
	);

	SET @state_id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='state_id'
	);

	SET @title =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='title'
	);

	SET @out_of_stock =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='out_of_stock'
	);
	
	SET @with_stock =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='with_stock'
	);	
	
	SET @active =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='active'
	);		
END;

--CONSTRUCT SQL
BEGIN
	--SQL BODY
	BEGIN
		SET @sql_body = '
		SELECT
		PRO.id,
		PRO.tenant_id,
		PRO.state_id,
		STA.title [state_title],
		PRO.currency_id,
		PRO.category_id,
		CAT.title [category_title],
		PRO.url_image,
		PRO.url_thumbnail,
		PRO.stock,
		PRO.selling_price_max,
		PRO.selling_price_min,
		PRO.code,
		PRO.title,
		PRO.url,
		PRO.enable_igv,
		PRO.enable_variant,
		PRO.active
		FROM
		(
			SELECT
			PRO.id,
			PRO.tenant_id,
			PRO.state_id,
			PRO.currency_id,
			PRO.category_id,
			(
				SELECT 
				PM.URL
				FROM dbo.product_media [PM]
				WHERE PM.product_id=PRO.id
				AND PM.principal=1
				AND PM.image_type=1
				AND PM.enable=1
			) [url_image],
			(
				SELECT 
				PM.URL
				FROM dbo.product_media [PM]
				WHERE PM.product_id=PRO.id
				AND PM.principal=1
				AND PM.image_type=2
				AND PM.enable=1
			) [url_thumbnail],
			(
				SELECT 
				SUM(V.stock)
				FROM dbo.variant [V]
				WHERE V.product_id=PRO.id
				AND V.active=1
			) [stock],
			(
				SELECT 
				MAX(V.selling_price)
				FROM dbo.variant [V]
				WHERE V.product_id=PRO.id
				AND V.active=1
			) [selling_price_max],
			(
				SELECT 
				MIN(V.selling_price)
				FROM dbo.variant [V]
				WHERE V.product_id=PRO.id
				AND V.active=1
			) [selling_price_min],
			PRO.code,
			PRO.title,
			PRO.url,
			PRO.enable_igv,
			PRO.enable_variant,
			PRO.active
			FROM dbo.product [PRO]
			WHERE PRO.tenant_id = @tenant_id
		) [PRO]';

		SET @sql_join = '
		INNER JOIN dbo.category [CAT] ON CAT.id=PRO.category_id
		INNER JOIN [compralope.parameters].dbo.state [STA] ON STA.id = PRO.state_id';
	END;

	--SQL WHERE
	BEGIN
		SET @sql_where = '
		WHERE (1=1)';

		--category_id
		IF @category_id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND PRO.category_id = @category_id';
		END;

		--list_category_id
		IF @list_category_id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND PRO.category_id IN (Select [value] from dbo.fn_split(@list_category_id,'',''))';
		END;

		--active
		IF @active IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND PRO.active = @active';
		END;

		--state_id
		IF @state_id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND PRO.state_id = @state_id';
		END;

		--out_of_stock
		IF @out_of_stock IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND PRO.stock = 0 ';
		END;

		--with_stock
		IF @with_stock IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND PRO.stock > 0 ';
		END;

		--title
		IF @title IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND PRO.title LIKE ''%'' + @title + ''%'' ';
		END;

	END;

	--ORDER BY
	BEGIN
		IF @order_by = ''
		BEGIN
			Set @order_by = 'id ASC';
		END;

		SET @order_by = '
		Order By ' + @order_by;
	END;
END;

--EXECUTE SQL
BEGIN
	SET @total_registers = 0;

	SET @sql_parameter = '@tenant_id int,@category_id int,@list_category_id VARCHAR(256),@active BIT,@state_id int,@out_of_stock BIT,@with_stock BIT,@title VARCHAR(256)';

	SET @sql_parameter_count = @sql_parameter + ',@total_registers int output';

	SET @sql = @sql_body + @sql_join + @sql_where;

	IF @page_active > 0
	BEGIN
		SET @sql_count = N'
		SELECT 
		@total_registers = COUNT(1)
		FROM
		(
			SELECT
			PRO.*,
			(
				SELECT 
				SUM(V.stock)
				FROM dbo.variant [V]
				WHERE V.product_id=PRO.id
				AND V.active=1
			) [stock]
			FROM dbo.product [PRO]
			WHERE PRO.tenant_id = @tenant_id
		) [PRO] ' + @sql_join + @sql_where;

		EXECUTE SYS.SP_EXECUTESQL @sql_count,@sql_parameter_count,@tenant_id,@category_id,@list_category_id,@active,@state_id,@out_of_stock,@with_stock,@title,@total_registers=@total_registers OUTPUT;

		SET @sql = 'SELECT * FROM (SELECT ROW_NUMBER() OVER (' + @order_by + ') [ROWNUM],* FROM (' + @sql + ') [TMP] ) [PAG] WHERE ROWNUM >= ' + @rownum_from + ' And ROWNUM <= ' + @rownum_to; 
	END
	ELSE
	BEGIN
		SET @sql += @order_by;
	END;

	--PRINT @sql;

	EXECUTE SYS.SP_EXECUTESQL @sql,@sql_parameter,@tenant_id,@category_id,@list_category_id,@active,@state_id,@out_of_stock,@with_stock,@title;
END;

GO