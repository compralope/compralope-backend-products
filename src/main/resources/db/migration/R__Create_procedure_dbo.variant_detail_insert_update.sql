-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.variant_detail_insert_update
(
	@id int OUTPUT,
	@variant_id int,
	@attribute_items_id int
)
AS
BEGIN

	IF NOT EXISTS (SELECT 1 FROM dbo.variant_detail WHERE id= @id)
	BEGIN
		INSERT INTO dbo.variant_detail
		(
			variant_id,
			attribute_items_id
		)
		VALUES
		(
			@variant_id,
			@attribute_items_id
		);

		SET @id = SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		UPDATE dbo.variant_detail SET
		variant_id = @variant_id,
		attribute_items_id = @attribute_items_id
		WHERE id = @id;
	END;

END;

GO