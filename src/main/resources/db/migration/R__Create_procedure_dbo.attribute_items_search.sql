-- ${flyway:timestamp}
CREATE OR ALTER PROCEDURE dbo.attribute_items_search
(
	@parameters_json ntext,
	@order_by varchar(128)
)
AS

--EXECUTE dbo.attribute_items_search '{"active":1}','';

DECLARE @sql nvarchar(MAX),@sql_body varchar(5500),@sql_join varchar(1000),@sql_where varchar(1500),@sql_parameter nvarchar(1024);

DECLARE @id INT,@active bit;

--FILTERS
BEGIN
	SET @id =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='id'
	);

	SET @active =
	(
		SELECT [value]
		FROM OPENJSON(@parameters_json)
		WHERE [key]='active'
	);
END;

--CONSTRUCT SQL
BEGIN
	--SQL BODY
	BEGIN
		SET @sql_body = '
		SELECT
		ATT.id,
		ATT.attribute_id,
		ATT.title,
		ATT.active';

		SET @sql_join = '
		FROM dbo.attribute_items [ATT]';
	END;

	--SQL WHERE
	BEGIN
		SET @sql_where = '
		WHERE (1=1)';

		--id
		IF @id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND ATT.id = @id';
		END;

		--active
		IF @id IS NOT NULL
		BEGIN
			SET @sql_where += + '
			AND ATT.active = @active';
		END;
	END;

	--ORDER BY
	BEGIN
		IF @order_by = ''
		BEGIN
			Set @order_by = 'id ASC';
		END;

		SET @order_by = '
		Order By ' + @order_by;
	END;
END;

--EXECUTE SQL
BEGIN
	SET @sql_parameter = '@id int,@active bit';

	SET @sql = @sql_body + @sql_join + @sql_where + @order_by;

	--PRINT @sql;

	EXECUTE SYS.SP_EXECUTESQL @sql,@sql_parameter,@id,@active;
END;

GO